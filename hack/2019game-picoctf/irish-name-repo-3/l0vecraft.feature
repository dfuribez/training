## Version 2.0
## language: en

Feature: Irish-Name-Repo-3 - web - picoCTF
  Code:
    Irish-Name-Repo-2
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name>    | <Version>              |
    | Ubuntu             | 18.04                  |
    | FireFox            | 72.0.1(64 bits)        |
    | BurpSuite Community| 2.1.07 (64 bits)       |
  Machine information:
    When I enter to the challenge I see the same page from another challenge
    Then I see the same menu
    And I enter to the admin login option, I see a single input "password"
    And I assume the challenge consist in bypass this login with the password

  Scenario: Fail: Trying a default password
    When I enter the password "admin"
    Then I intercept the request and change the "debug"param to "1"
    And I see this result "nqzva" also a fail message

  Scenario: Success: caesar cypher and sql injection
    When I write "abcdefghijkmnopqrstuvwxyz"
    Then I see this result "nopqrstuvwxzabcdefghijklm"
    When I search the "caesar cypher" I notice the "ROT13" output it's the same
    Then I apply the cypher to the bypass
    And the result is
    """
    'be 1 = 1 --
    """
    When I send a request with the bypass
    Then I see the result of the sql injection
    """
    password=''or 1 = 1 --
    """
    And I also see the flag, now I can finish the challenge
