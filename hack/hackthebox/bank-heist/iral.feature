## Version 2.0
## language: en

Feature: bank_heist-crypto-hackthebox
  Site:
    https://www.hackthebox.eu
  Category:
    crypto
  User:
     iral
  Goal:
    Find the flag on the given zip

  Background:
  Hacker's software:
    | <Software name> | <Version>           |
    | Linux           | 4.19.0-kali1-amd64  |
    | Firefox esr     | 60.8.0esr           |

  Scenario: Success: ctf
    Given I download the file called bank-heist.zip
    When I proceed to decompress
    Then throw a txt file called bank-heist
    And I find within the file the following message
    """
    444333 99966688 277733 7773323444664 84433 22244474433777,
    99966688 277733 666552999. 99966688777 777744277733 666333
    84433 443344477778 4447777 44466 99966688777 4466688777733.
    84433 5533999 8666 84433 55566622255 4447777 22335556669.
    4666 8666 727774447777.

    47777888 995559888 4555 47777888 44999988 666555997 :
    8555444888477744488866888648833369!!
    """
    When I realize that the description refers to a cell phone message
    Then I decide to investigate websites deciphering text messages
    And I found the following web page
    """
    https://www.dcode.fr/phone-keypad-cipher
    """
    When I enter the code I throw the following result
    """
    IF YOU ARE READING THE CIPHER, YOU ARE OKAY.
    YOUR SHARE OF THE  HEIST IS IN YOUR HOUSE.

    THE KEY TO THE LOCK IS BELOW. GO TO PARIS:
    GSV XLWV GL GSV HZU OLXP TLIVGRIVNVMGUFMW!!
    """
    Then I see that the last part of the message is still weird
    Then I decide to look for a text analysis website
    And I found the following web page
    """
    https://www.boxentriq.com/code-breaking/text-analysis
    """
    And recommend websites that could help me
    """
    https://www.boxentriq.com/code-breaking/cryptogram
    https://www.boxentriq.com/code-breaking/affine-cipher
    https://www.boxentriq.com/code-breaking/atbash-cipher
    https://www.boxentriq.com/code-breaking/rot13
    """
    And I proceed to review the following website
    """
    https://www.boxentriq.com/code-breaking/atbash-cipher
    """
    Then I get the following result
    """
    THE CODE TO THE SAF LOCK <REDACTED>!!
    """
    And I get the flag :)
