## Version 2.0
## language: en

Feature: 16 - Web Design - Yashira
  Code:
    16
  Site:
    www.yashira.org
  Category:
    Web Design
  User:
    fgomezoso
  Goal:
    Find the wrong HTML tag inside a website

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 10           |
     | Chrome          | 76.0.3809.132|
  Machine information:
    Given I am accessing the challenge site via browser
    """
    http://www.yashira.org/ReTos/All/prueba1.html
    """

  Scenario: Fail:Swap HTML Tags
    Given the main page is shown
    When I go to the source code
    Then I can see there is line with a div tag inside a font tag
    Then I swap both HTML tags
    And I copy/paste the edited line into the challenge page
    But the line did not work
    And I could not pass the challenge

  Scenario: Success:Validate Website
    Given I use the w3c's validator website
    """
    https://validator.w3.org
    """
    When I check the challenge's address
    Then I can see a list of errors
    Then I look for not allowed attributes inside the HTML tags
    Then I test the attributes by changing their values
    And I try to see if the page is changed
    And there is an attribute where the page has no changes
    """
    Error: Attribute border not allowed on element hr at this point.

    From line 29, column 1; to line 29, column 54

    ↩</table>↩<hr width="95%" align="center" color="red" border="4">↩<!--
    """
    Then I erase the attribute border from hr tag
    When I copy/paste the edited line into the challenge page
    """
    <hr width="95%" align="center" color="red">
    """
    Then I solved the challenge
