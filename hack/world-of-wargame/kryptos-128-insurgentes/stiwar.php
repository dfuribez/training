<?php

    $cifrado = ' -fMha9Z,';
    $matriz = str_split($cifrado);
    $rec = '';
    for ($i = 0; $i < count($matriz); $i++) {
        if($i == 0){
            $rec .= str_pad(decbin(ord($matriz[$i])), 8, '0', STR_PAD_LEFT);
        }elseif ($i == 1){
            $rec .= str_pad(decbin(ord($matriz[$i])), 6, '0', STR_PAD_LEFT);
        }else{
            $rec .= str_pad(decbin(ord($matriz[$i])), 7, '0', STR_PAD_LEFT);
        }
    }
    $j=0;
    $c = strlen($rec);
    $out = array();
    $ini = 0;
    while ($c >0){
            $out[$j] = substr($rec, $ini,8);
            $c -= 8;
            $ini += 8;
            $j++;
    }
    echo 'solucion: <br>';
    for ($i = 1; $i < count($out)-1; $i++) {
        echo chr(bindec($out[$i]));
    }
?>