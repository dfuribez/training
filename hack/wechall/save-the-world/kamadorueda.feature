## Version 2.0
## language: en

Feature: Crypto-We Chall
  Site:
    We Chall
  Category:
    Crypto
  User:
    kedavamaru

  Background:
  Hacker's software:
    | <Software name> | <Version>                 |
    | Ubuntu          | 18.04.1 LTS (amd64)       |
    | Google Chrome   | 70.0.3538.77 (64-bit)     |
  Machine information:
    Given I'm accesing the challenge page
    And the statement is displayed
    """
    It is year 2018, the world war III is upcoming between USA and China. You
    are a secret agent working for the USA. The USA IGA (Information Gathering
    Agency) gathered three RSA enciphered messages. All messages were originated
    from the same "clear text message", which is a symmetric key (m). With this
    symmetric key there is a document encrypted. This document contains all the
    secret information about the newest high-tech weapons made in China. This
    document was symmetrically encrypted with the CSEA (Chinese Super Encryption
    Algorythm). This algorythm is almost unbreakable.

    You know, that the chinese government used RSA to encrypt the symmetric key.
    Your boss, general Eromzig bought a new supercomputer to factorize
    the RSA public keys. Scientists say they need 2 weeks to factorize one of
    the public keys.
    That's why he hired you, because he thinks you can break the RSA algorithm
    in 1 hour. Your mission is to get the symmetric key in decimal format.
    """
    And three encrypted asymmetric RSA messages are known
    And three public keys are known
    And the exponents are the same for the three receivers
    Then the RSA encryption equations are
    """
    c1 = m**e mod n1, for general Sunt
    c2 = m**e mod n2, for general Qin
    c3 = m**e mod n3, for general Ymenetn
    """

  Scenario: Fail: assume the modulo is discardable
    Given I assume the the dividend is less than the divisor
    """
    m**e >= n
    """
    And I compute with a python script
    """
    m1 = cubic_root(c1)
    m2 = cubic_root(c2)
    m3 = cubic_root(c3)
    """
    Then I found that m1 != m2 != m3
    And I conclude the condition is not fulfilled

  Scenario: Fail: mathematical based - brute force
    Given the asymmetric RSA equation is
    """
    c = m**e mod n
    """
    And I assume the the dividend is greater than the divisor
    """
    m**e >= n
    """
    Then I can apply some math to simplify the problem
    Given m is an integer (objective)
    And c is an integer (encrypted message)
    And n is an integer (public key)
    And k is an integer (quotient)
    And r is an integer (remainder)
    Then one can find unique k, and r, such that
    """
    m**e = k * n + r
    """
    And symplifying
    """
    m**e = k * n + r
    m**e = k * n + (m**e % n)
    m**e = k * n + c
    m = (k * n + c) ** (1 / e)
    """
    # for k = 0, this is the method 1
    # but we are now free to assume that m**e >= n
    When I compute a brute force with this method in python
    Then I conclude that there is no solution for a single 'k' to the system
    """
    m = (k * n1 + c1) ** (1 / e)
    m = (k * n2 + c2) ** (1 / e)
    m = (k * n3 + c3) ** (1 / e)
    """
    But I conclude that there is solution in a set of 'k' to the system
    """
    m = (k1 * n1 + c1) ** (1 / e)
    m = (k2 * n2 + c2) ** (1 / e)
    m = (k3 * n3 + c3) ** (1 / e)
    """

  Scenario: Fail: mathematical based - inverse equalization
    Given I learned that the offset 'k' is different for all equations
    Then I propose to solve the following system
    """
    m1 = (k1 * n1 + c1) ** (1 / e)
    m2 = (k2 * n2 + c2) ** (1 / e)
    m3 = (k3 * n3 + c3) ** (1 / e)
    such that m1 == m2 == m3
    """
    And I construct a code in python starting from
    """
    k1 = 1000000000
    k2 = 1000000000
    k3 = 1000000000
    """
    And decreasing each 'k' individually trying to make the messages equal
    When I run the previous code
    Then I see that there is not a solution on the proposed range
    And that a brute force approach may be infeasible in less than some minutes
    And that I should try another approach

  Scenario: Success: exploit known research about RSA
    Given I do some research about the vulnerabilities of RSA
    Then I found a method that match the problem conditions
    """
    Coppersmith's attack
    -- Hastad's broadcast attack
    """
    Given the message 'm' (message) to 'e' (exponent) recipients
    Then RSA is not safe
    Given Ni are pairwise coprime
    And gcd(Ni, Nj) = 1 for all i, j
    And C1 = c % N1
    And C2 = c % N2
    And C3 = c % N3
    Then I can compute 'c' using the chinese remainder theorem
    And c = m^3 % (N1*N2*N3)
    But c = m^3, because RSA conditions
    When I put the previous pseudocode in a python script
    Then I get the following results
    """
    test 4: hastad's broadcast attack
      verification 1 passed
      verification 2 passed
      verification 3 passed
    enter this on wechall: 21987654321987654321
    """
    And I solve the challenge
