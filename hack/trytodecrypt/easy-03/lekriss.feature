## Version 1.0
## language: en

Feature: 1-cryptography-trytodecrypt.com
  Site:
    trytodecrypt.com
  Category:
    cryptography
  User:
    Cristian Guatuzmal
  Goal:
    Discover the decrypted text

    Background:
  Hacker's software:
    |<software>            |<version>       |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  Machine information:
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=2
    """
    Then I open the URL with Mozilla
    And I see the challenge statement
    """
    For the beginning here you can find some easy crypto stuff
    """
    And I see the crypted text
    """
    0A0B1339150B1139070A0B13390510
    """
    And an input format which allows me to cipher text
    And uses the same cipher pattern as the text

  Scenario: Success:building the cipher pattern
    Given the web page has an input field and an Encrypt Button
    Then I start to put some values in the input field
    And I start to encrypt them
    Then I found the encryptation pattern
    And it is an incremental code which goes like this
    """
    FC-FE[abc]
    00-09[defghijklm]
    0A-0F[nopqrs]
    10-16[tuvwxyz]
    39[" "]
    """
    And every position correlates with a character
    Then I use the cipher pattern to traduct the crypted message
    And I decode the message which says(now you know it)
    And I run it in the solution field
    Then I can see that the message is correct
    Then I conclude that my solution is the right decrypt pattern
    And I solved te challenge
