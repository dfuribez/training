## Version 2.0
## language: en

Feature: dexters-lab - web - ctftime
  Site:
    www.ctftime.org
  Category:
    Web
  User:
    fgomezoso
  Goal:
    Login to the website and get the hidden flag

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 10           |
     | Chrome          | 76.0.3809.132|
  Machine information:
    Given I am accessing the challenge site via browser
    """
    http://dexterslab.web.2019.nactf.com/
    """
    And the username is "Dexter"

  Scenario: Fail:Input single quote as password
    Given the main page is shown
    When I enter a single quote (') in the password field
    And I try to log in
    Then I get an error page with a message
    """
    Warning: mysqli_num_rows() expects parameter 1 to be mysqli_result,
    bool given in /var/www/html/login.php on line 93

    Nice try, Dee Dee.Nice try, Dee Dee.
    """
    But there is no hidden flag
    And I could not pass the challenge

  Scenario: Success:SQL injection with a true statement
    Given the password must be valid
    When I inject a SQL command with a boolean condition
    And the condition is always true
    """
     ' or '1'='1
    """
    Then I get a new page with the hidden flag
    And I solved the challenge
