## Version 2.0
## language: en

Feature: web-ninja -web -backdoor
  Site:
    backdoor
  Category:
    Web
  User:
    alejandrohg7
  Goal:
    Find the hidden flag in the form

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |
  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: Fail: cookie manipulation
    Given I am logged in the challenge page
    When I look the source code of the challenge page
    Then I can not see the cookies stored in my browser
    And I can not get the flag

  Scenario: Success: form injection
    Given there is a form in the challenge page
    When I try a sample injection "{{1+1}}" in the form
    Then there is some interesting results "2"
    When I inject "{{config}}"
    Then I get the flag
