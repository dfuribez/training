## Version 2.0
## language: en

Feature: Be alert - miscellaneous - rankk
  Site:
    Rankk
  User:
    pperez7 (wechall)
  Goal:
    Solve the challenge

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Windows         |     1809      |
    | Google Chrome   | 79.0.3945.130 |
  Machine information:
    Given a link in the challenge's page
    When I access it

  Scenario: Fail: Playing the game
    Given The table input and the buttons that are at the page
    """
    [evidence](game.png)
    """
    When I select "deeppink" from the colors list
    Then I press start button
    When I press stop very fast
    Then a password input appears
    And I try "colors" as password
    And the page redirects to the same challenge's page

  Scenario: Success: Analyzing source code
    Given The challenge's source code
    When I see there is a link to a Javascript code
    """
    js/reflex.js
    """
    Then I go into the file
    And I see the code that the challenge is using to perform the game
    When I look closely to the code
    Then I realize that there is a function called "pass()"
    """
    function pass() {
    var key = "yz5w16dw28c34g9266j7";
    var py = "be-alert.py";
    var interpret = "";
    var whatisthis = "var xorm = prompt('Enter the password:','');
    for (x=1; x<6; x++) {
      if (x != 5){interpret += (key.indexOf(x));
      }
      else{
        interpret += 7;
      }
    }
    interpret += 8;
    if (xorm==interpret){
      interpret = py + '?code=' + interpret;
      location.href=interpret;
    }
    else{
      window.location.href=py;
    }";
    eval(whatisthis);
    }
    """
    Then I take the part of the code where "var interpret" is being generated
    """
    var key = "yz5w16dw28c34g9266j7";
    var interpret = "";
    for (x=1; x<6; x++) {
      if (x != 5){interpret += (key.indexOf(x));
      }
      else{
        interpret += 7;
      }
    }
    interpret += 8;
    """
    When I go to "paiza.io" where I can find a Javascript code compiler
    Then I paste the code in the editor
    And I use the "console.log()" function to get the value of "interpret"
    And I get the <FLAG>
    Given the Javascript code
    When I look closely to the "pass()" function
    Then I see that there is a "code" parameter for the challenge
    When I perform the next request
    """
    https://www.rankk.org/challenges/be-alert.py?code=<FLAG>
    """
    Then I solve the challenge
