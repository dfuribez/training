# pylint pontifex.py
# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
Code to solve the problem Pontifex from rankk
"""

# string to decode
CIPHER_STRING = "KGSJR IKSVC WUQWL HFLKV HCEMS DVUFS"

# ALPHABET to convert from number to char
ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

# KEYS from the image containing the cards
KEYS = [{"key": 4, "suit": "diamond"},
        {"key": 12, "suit": "diamond"},
        {"key": 1, "suit": "diamond"},
        {"key": 4, "suit": "diamond"},
        {"key": 3, "suit": "heart"},
        {"key": 10, "suit": "spade"},
        {"key": 3, "suit": "diamond"},
        {"key": 12, "suit": "diamond"},
        {"key": 13, "suit": "club"},
        {"key": 1, "suit": "spade"},
        {"key": 9, "suit": "heart"},
        {"key": 12, "suit": "heart"},
        {"key": 11, "suit": "spade"},
        {"key": 7, "suit": "diamond"},
        {"key": 7, "suit": "spade"},
        {"key": 9, "suit": "heart"},
        {"key": 3, "suit": "spade"},
        {"key": 5, "suit": "spade"},
        {"key": 9, "suit": "spade"},
        {"key": 8, "suit": "heart"},
        {"key": 6, "suit": "diamond"},
        {"key": 3, "suit": "diamond"},
        {"key": 9, "suit": "spade"},
        {"key": 10, "suit": "club"},
        {"key": 4, "suit": "heart"},
        {"key": 3, "suit": "diamond"},
        {"key": 11, "suit": "spade"},
        {"key": 10, "suit": "diamond"},
        {"key": 8, "suit": "club"},
        {"key": 8, "suit": "spade"}]


# value to count current key
i = 0

# var to store solution
SOL = ""

# iterate over chars in cipher string
for c in CIPHER_STRING:
    if c != " ":

        # get the current key
        current_key = KEYS[i]

        # check suit of current key to convert it to number value
        if current_key['suit'] == "club":

            key_stroke = current_key['key'] + 0

        elif current_key['suit'] == "diamond":

            key_stroke = current_key['key'] + 13

        elif current_key['suit'] == "heart":

            key_stroke = current_key['key'] + 26

        elif current_key['suit'] == "spade":

            key_stroke = current_key['key'] + 39

        # get the position from the current cipher char
        cipher_value = (ALPHABET.find(c) + 1)

        # substract to get the position of decoded string
        value = cipher_value - key_stroke

        # add 26 if the value is negative
        if value < 0:
            value += 26

        SOL += ALPHABET[value - 1]

        i += 1
print "The solution is "+SOL
# python pontifex.py
# The solution is <flag>
