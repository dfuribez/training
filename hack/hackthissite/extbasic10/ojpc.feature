# language: en

Feature: Solve the challenge 10
  From the hacksithissite.org website
  From the extbasic category
  With my username siegfrieg94

  Background:
    Given a batch script authentication system
    And I have to avoid the passsword authentication
    And I am running the linux distribution, Ubuntu 16.04 LTS
    And I already solve the challenge 11 so I already have an idea about problem solution

  Scenario: Successful Solution
    When I analyze the batch script
    Then I find that a comparison is required to authenticate the  password
    Then I know that I need to change the comparison to a value known for me 
    Then I change a line in the batch script
    And I solve the challenge 

