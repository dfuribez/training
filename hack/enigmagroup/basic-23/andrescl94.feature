# language: en

Feature: Solve the challenge Basic 23
  Identified with code 23
  From the site www.enigmagroup.org
  From the category Basic, SQL Injection Challenges
  With my username AndresCL94

Background:
  Given that I am registered in the website enigmagroup.org
  And I am running the OS Windows 7 Ultimate, version x64
  And I have Google Chrome 60.0.3112.113 installed as the default web explorer
  And I have access to the internet
  And I have previously solved one basic SQL challenges using the union command

Scenario: Failed Attempt
  Given that I am trying to find a valid username and password to log in
  And I notice in the URL that the website sorts its pages using a variable "id"
  And I learned from the source code that id ranges from 1 to 4
  When I change in the URL the id number for one out of range
  And add the command "union select 1,2,3"
  Then I get a warning saying that a SQL injection attack was attempted
  But I notice that one of the indexed pages is called "Hacked"
  And I read that a SQL Injection filter has been placed
  Then I decide to read about these types of filters

Scenario: Successful Solution
  Given that I am trying to find a valid username and password to log in
  And I have taken into account the results of the previous scenario
  And I have found a type of filter that does not allow the use of whitespaces
  And I have learned that comment marks can be used instead of whitespaces
  When I run the command /**/union/**/select/**/1,2,3 after the id number
  Then I visit a website that has the numbers 2 and 3
  And I assume that the column names are "username" and "password"
  When I run the command /**/union/**/select/**/1,username,password
  Then I see that the username is "administrator"
  And the password is an apparently hashed value
  Then I use an online md5 decrypter to decipher the password, "asdfgh"
  When I input "administrator" and "asdfgh" in the login page
  Then I solve the challenge
