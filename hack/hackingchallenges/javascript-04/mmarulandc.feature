## Version 2.0
## language: en

Feature: javascript-4 - javascript - hacking-challenges
  Site:
    hacking-challenges
  Category:
    javascript
  User:
    mmarulandc
  Goal:
    Enter the correct password

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Windows OS      | 10           |
    | Chrome          | 78.0.3904.70 |
  Machine information:
    Given I am accessing the challenge site via browser
    And the site has one password field

  Scenario: Success:Finding values in javascript
    Given the challenge page is displayed
    When I check the source code
    Then I find a script with a password function
      """
      function pwd() {
        code  = "Schreiptisch";
        code2 = "9182736450";
        code3 = code2.indexOf("91");
        code4 = code2.indexOf("18");
        code5 = code2.indexOf("18");
        code6 = code2.indexOf("82");
        code7 = code2.indexOf("27");
        code8 = code2.indexOf("73");
        code9 = code2.indexOf("36");
        code10= code2.indexOf("64");
        code11= code2.indexOf("64");
        code12= code2.indexOf("45");
        code13= code2.indexOf("45");
        code14= code2.indexOf("50");
        Pass2 = code.substring(code3,code4);
        Pass3 = code.substring(code5,code6);
        Pass4 = code.substring(code7,code8);
        Pass5 = code.substring(code9,code10);
        Pass6 = code.substring(code11,code12);
        Pass7 = code.substring(code13,code14);
        passwort = Pass2 + Pass3 + Pass4 + Pass5 + Pass6 + Pass7;
        if (passwort== document.hackit.eingabe.value) {
          alert ("Super das Passwort ist richtig!");
        }
        else {
          alert("Das Passwort ist Falsch!");
        }
      }
      """
    And I find out that there is a conditional blocking the access
    And I need to find the right value for the variable
    When I ran the following code with Chrome devtools
      """
      code  = "Schreiptisch";
      code2 = "9182736450";
      code3 = code2.indexOf("91");
      code4 = code2.indexOf("18");
      code5 = code2.indexOf("18");
      code6 = code2.indexOf("82");
      code7 = code2.indexOf("27");
      code8 = code2.indexOf("73");
      code9 = code2.indexOf("36");
      code10= code2.indexOf("64");
      code11= code2.indexOf("64");
      code12= code2.indexOf("45");
      code13= code2.indexOf("45");
      code14= code2.indexOf("50");
      Pass2 = code.substring(code3,code4);
      Pass3 = code.substring(code5,code6);
      Pass4 = code.substring(code7,code8);
      Pass5 = code.substring(code9,code10);
      Pass6 = code.substring(code11,code12);
      Pass7 = code.substring(code13,code14);
      passwort = Pass2 + Pass3 + Pass4 + Pass5 + Pass6 + Pass7;
      console.log(passwort)
      """
    Then I find the value for the password
    When I enter the password
    And the form is submitted
    Then a success message is displayed
    And I solve the challenge
