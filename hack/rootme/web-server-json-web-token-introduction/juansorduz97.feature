## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    rootme
  Category:
    Web server
  User:
    juansorduz97
  Goal:
    Connect as admin

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04.6     |
    | Mozilla         | 71.0        |
    | Burp Suite      | 2.1.07      |
  Machine information:
    Given a link in the challenge page
    When I access it
    Then I can see the following message
    """
    Statement

    To validate the challenge, connect as admin.
    """

  Scenario:  Fail: Put modified JSON hash without change encoding algorithm
    Given The challenge page show a login
    When I insert "admin" as user and "admin" as password
    Then The challenge page reload without visible changes
    When I press "login as a guest"
    Then The challenge page change
    And It greet me as guest
    When I reload the challenge page and capture the packets with Burp Suite
    Then I see a GET request with a cookie "jwt"
    When I check the challenge page problem statement
    Then I see the tool https://jwt.io/
    When I enter the web page https://jwt.io/
    Then I see a encoded-decoded JSON web token
    Then I put the cookie "jwt"
    And I see the following decoded information
    """
    Header:
    {
      "typ": "JWT",
      "alg": "HS256"
    }
    PayLoad:
    {
      "username": "guest"
    }
    """
    When I change "username" to "admin"
    Then I obtain a new hash
    When I change the cookie "jwt" in Burp Suite for the new hash
    And I send the new GET request
    Then I obtained a message "Wrong signature"

  Scenario:  Success: Put JSON hash without encoding algorithm
    Given The challenge page does not accept the my new JSON hash
    When I check the challenge page problem statement again
    Then I see an interesing additional resource with the following name
    """
      Attacking JWT authentication - Sjoerd Langkemper
    """
    When I open the resource
    Then I see a document about atacking jwt authentication
    When I check the document
    Then I see interesting information
    """
      JWT consist of base64-encoded data
      JWT consist of three parts separeted by dots
      JWT libraries should support an special none algorithm
    """
    Then I open the following tool https://www.base64encode.org/
    Then I encode the following two parts separeted with dots
    """
    Header:
    {
      "typ": "JWT",
      "alg": "none"
    }
    PayLoad:
    {
      "username": "admin"
    }
    """
    And I put a dot at the end of the new hash
    When I change the cookie "jwt" in Burp Suite for the new hash
    And I send the new request
    Then the challenge page recognize me as admin
    And It show me a flag [evidence](requestnewhash.png)
    When I put the flag in the challenge page problem statement
    Then I solved the challenge
