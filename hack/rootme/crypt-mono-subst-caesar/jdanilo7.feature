## Version 2.0
## language: en

Feature:  Web Client - Root Me
  Site:
    www.root-me.org
  Category:
    Cryptanalysis
  User:
    jdanilo7
  Goal:
    Decrypt a monoalphabetic substittion ciphered message

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |    60.2.0esr    |
  Machine information:
    Given The challenge url
    """
    https://www.root-me.org/en/Challenges/Cryptanalysis/Monoalphabetic-
    substitution-Caesar
    """
    And The challenge statement
    """
    We just caught the messenger of the Emperor. He transmitted a coded
    message to his son. This could be an important message. You’ve to decrypt
    it ! To validate, you must enter the concatenation of the first letters of
    each line followed by the concatenation of the last letters of each line
    (for example : tfhqdlhfpkmeokgq).
    tm bcsv qolfp
    f'dmvd xuhm exl tgak
    hlrkiv sydg hxm
    qiswzzwf qrf oqdueqe
    dpae resd wndo
    liva bu vgtokx sjzk
    hmb rqch fqwbg
    fmmft seront sntsdr pmsecq
    """
    Then I am asked to decrypt the message.

  Scenario: Fail: Decode the entire message with the same amount of shifts
    Given the challenge message
    And this Caesar cipher decoder
    """
    https://cryptii.com/pipes/caesar-cipher
    """
    Then I try to decipher the message with all possible shifts from 1 to 25
    And I don't get any decrypted message.

  Scenario: Success: Use a different number of shifts for each word
    Given the challenge message
    And this Caesar cipher decoder
    """
    https://cryptii.com/pipes/caesar-cipher
    """
    Then I notice that some french words are decoded with certain shifts
    Given I check all the words one by one with each shift from -2 to 25
    Then I notice every word is encoded with a different number of shifts
    And the first word is encoded with 25 shifts
    And the last word is encoded with 2 left shifts (-2)
    And the decoded message is
    """
    un deux trois
    j'irai dans les bois
    quatre cinq six
    cueillir des cerises
    sept huit neuf
    dans un panier neuf
    dix onze douze
    elles seront toutes rouges
    """
    Given I form the password according to the challenge message
    """
    ujqcsddessxsffes
    """
    Then I validate it on the challenge website
    And it is accepted
