## Version 2.0
## language: en

Feature: hackburger
  Site:
    hackburguer
  User:
    charlie517 (wechall)
  Goal:
    get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Linux Mint      | 4.15.0-45   |
    | Firefox         | 65.0.1      |
  Machine information:
    Given I am accessing the hacktheburger website
    And PHP version 5.6.39-0
    And is running on Ubuntu 4.9.0-7

  Scenario: Fail:read flag.php file
    Given I'm on the webpage
    And I print every file on the current folder
    Then I can see flag.php file
    Then I try to read it
    And get empty file
    Then I conclude that I can't use read the file this way.
    And cat the file did not work
    And I could not capture the flag

  Scenario: Success: cat the file and inspect the HTML source code.
    Given I print flag.php file
    Then I check the source HTML code
    And get the congratulations message and the flag
    Then I can actually read the flag from the HTML source code
    And I conclude that worked
    And solved the challenge
    And I caught the flag
