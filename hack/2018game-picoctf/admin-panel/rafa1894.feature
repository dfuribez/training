## Version 2.0
## language: en

Feature: admin-panel-Forensics-2018game-picoctf
  Code:
    admin panel
  Site:
    2018game-picoctf
  Category:
    Forensics
  User:
    rferi1894
  Goal:
    Analyze the file, get the password and catch the flag.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.2 LTS |
    | Wireshark       | 2.6.8       |

  Machine information:
    Given the challenge URL
    """
    https://2018game.picoctf.com/problems
    """
    And the challenge information
    """
    We captured some traffic logging into the admin panel,
    can you find the password?
    """"
    And the file provided "data.pcap."
    And the field to submit the flag

  Scenario: Fail:Wireshark
    When I download the file I see the extension .pcap
    Then I investigate about these kinds of files
    And I find out that you can analyze them with wireshark
    Then I install wireshark in my system
    And open the file with it
    Then I can see a huge amount of information
    And I search for the flag for quite a while
    Then I decide that with so much information this is inefficient
    And I begin trying to get the flag another way

  Scenario: Success:strings-and-grep
    When I use the string command on the file
    Then I can see that it has so much info it fills the terminal
    And I also try using grep but it doesn't find the flag
    Then I try to pipe the strings output directly to grep
    And this time it finds the words I'm looking for
    Then I catch the flag
    And I solve the challenge
