## Version 2.0
## language: en

Feature: Logon-WebExploitation-2018game-picoctf
  Code:
    Logon
  Site:
    2018game-picoctf
  Category:
    Web Exploitation
  User:
    rferi1894
  Goal:
    Become an admin and get the flag.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.2 LTS |
    | Google Chrome   | 74.0.3729   |

  Machine information:
    Given the challenge URL
    """
    http://2018shell.picoctf.com:6153
    """
    And the challenge information
    """
    I made a website so now you can log on to! I don't
    seem to have the admin password. See if you can't
    get to the flag.
    """"
    And the field to submit the flag

  Scenario: Fail:SQLi-bypass-login
    When I try entering arbitrary login credentials
    Then I successfully login
    And I see that this way I can't be admin
    Then I try to login with admin username
    And this time I can't log in
    Then I conclude that the page only checks admin password
    And I try to bypass login with the following SQL injection
    """
    'or true --
    """
    And I still can't log in

  Scenario: Success:cookie-editor
    When I inspect the page
    Then I can see that it's storing cookies
    And I log in with arbitrary login credentials again
    And I can see cookies without encryption
    And one of them is called admin
    Then I change the cookie's value from False to True
    And reload the page
    And I become admin
    Then I catch the flag
    And I solve the challenge
