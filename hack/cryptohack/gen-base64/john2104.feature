## Version 2.0
## language: en

Feature: general-base64-cryptohack
  Site:
    https://cryptohack.org/challenges/general/
  Category:
    Crypto
  User:
    john2104
  Goal:
    Decrypt the text

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    https://cryptohack.org/challenges/general/
    """
    When I open the url with Firefox
    Then I see a string of numbers and letters
    """
    72bca9b68fc16ac7beeb8f849dca1d8a783e8acf9679bf9269f7bf
    """
  Scenario: Fail:decrypt
    Given the array
    Then I try to decypher it converting it from hex value
    When I put the string I get:
    """
      r¼©¶ÁjÇ¾ëÊx>Ïy¿i÷¿
    """
    And I didn't solve it

   Scenario: Success:Decrypt
    Given the array
    Then I go to the terminal
    And convert the string to bytes
    And I convert it to base64 string using
    """
      echo "72bca9b68fc16ac7beeb8f849dca1d8a783e8acf9679bf9269f7bf" | xxd -r
      -p | base64
    """
    Then I get the flag
    And I solve the challenge
