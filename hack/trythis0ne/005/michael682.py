""" Scripts for reverseme challenge
"""
# BINARYReverseOrder
VALUE = 133701337
BINARY = bin(VALUE)
BINARY = BINARY[:1:-1]
print(int(BINARY, 2))

# nbyteReverseOrder
VALUE = 133701337
VALUE = str(VALUE)[::-1]
print(VALUE)


# BruteForce
def newnbyte(byte, val):
    """ Calculate the new byte
    """
    val = int(byte) + val
    if val <= 0:
        return str(9 + val)
    return str(val)


VALUE = 133701337
VALUE = str(VALUE)
VALUEF = ''
for index, nbyte in enumerate(VALUE):
    if index == 0:
        VALUEF += nbyte
    if index == 1:
        VALUEF += newnbyte(nbyte, -1)
    if index == 2:
        VALUEF += newnbyte(nbyte, -7)
    if index == 3:
        VALUEF += newnbyte(nbyte, -3)
    if index == 4:
        VALUEF += newnbyte(nbyte, -5)
    if index == 5:
        VALUEF += newnbyte(nbyte, -4)
    if index == 6:
        VALUEF += newnbyte(nbyte, -3)
    if index == 7:
        VALUEF += newnbyte(nbyte, -6)
    if index == 8:
        VALUEF += newnbyte(nbyte, -2)
print(VALUEF)
