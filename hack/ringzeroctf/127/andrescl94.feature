## Version 2.0
## language: en

Feature: basic-linux-shellcoding
  Site:
    https://ringzer0ctf.com
  Category:
    shellcoding
  User:
    AndresCL94
  Goal:
    Execute shellcode to get the flag

  Background:
  Hacker's software:
  | <Software name>                | <Version> |
  | Ubuntu (Bionic)                | 18.04.1   |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/127
    """
    And the SSH credentials that can be found in
    """
    host: challenges.ringzer0team.com
    port: 10127
    user: level1
    pass: 50g8O1R0C42nP7N
    """
    When I connent to the server through SSH
    Then I see a console asking me to input shellcode in hexadecimal format

  Scenario: Success: Create-Shell
    Given the challenge instructions
    Then I start investigating about shellcoding
    And I come across the following exploit
    """
    https://www.exploit-db.com/shellcodes/46907
    """
    When I enter the hexadecimal code
    Then I obtain an interactive shell to execute command
    When I list the current directory
    Then I see the file that has the flag
    And I solve the challenge
