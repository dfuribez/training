## Version 2.0
## language: en

Feature: fashion-victim
  Site:
    https://ringzer0ctf.com
  Category:
    cryptography
  User:
    AndresCL94
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version> |
  | Ubuntu (Bionic) | 18.04.1   |
  | ImageMagick     | 6.9.7-4   |
  | Python          | 3.6.9     |
  | Pillow          | 5.1.0     |
  | Numpy           | 1.18.2    |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/156
    """
    Then I see a button to start the challenge
    And I get redirected to a site that has a GIF of an Apple TV

  Scenario: Fail: XOR
    Given that the GIF is the most outstanding element
    Then I decide to download it
    When I investigate a little about Steganography
    Then I find an idea of extracting the frames of the GIF and XORing them
    When I use ImageMagick's "convert" command
    Then 30 frames of the GIF are extracted
    And I use Python's "Pillow" and "Numpy" packages to XOR them according to
    """
    https://stackoverflow.com/questions/54398627/xor-ing-and-summing-two-
    black-and-white-images
    """
    When I analyze the result
    Then I cannot find anything relevant to solve the challenge

  Scenario: Success: Associative-XOR
   Given the previous approach did not yield the result
   And that I XORed all frames into a single result
   Then I decie to XOR only two frames at a time
   And try every single combination of the 30 frames
   When I run a script for this purpose [exploit.py]
   Then I get all the possible combinations of XORing two frames
   And I start browsing through them
   And I find the flag
