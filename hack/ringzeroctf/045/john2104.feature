## Version 2.0
## language: en

Feature: headache-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/45
  Category:
    Web
  User:
    john2104
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/45
    """
    When I open the url with Chrome
    Then I see a text "You don't have admin access."
  Scenario: Fail:Inspect
    Given the url
    Then I check the cookie "AUTH"
    And decode it using base64
    Then I see the string
    """
    guest,e5ff2722f8e6bf2f,1587504919,false:0e47b50adf6230c33e4e04e2052e21ce
    """
    Then I change the user to admin
    And the boolean to true
    And the time in millis to "1987504919"
    And encode it again
    """
    admin,e5ff2722f8e6bf2f,1987504919,true:0e47b50adf6230c33e4e04e2052e21ce

    YWRtaW4sZTVmZjI3MjJmOGU2YmYyZiwxNTg3NTA0OTE5LHRydWU6MGU0N2I1MGFkZjYyMzBjMz
    NlNGUwNGUyMDUyZTIxY2U=
    """
    Then I get the message
    """
    Look's like someone is trying to hack our system (Invalid MD5).
    """
    And I don't solve the challenge

   Scenario: Success:MD5
    Given that I inspected the cookie
    When I tried to get the MD5 hash of
    """
    admin,e5ff2722f8e6bf2f,1987504919,true
    """
    Then I get "ac3c962632bd1f68d0906c05d82d507c"
    And put that as the last part of the cookie
    Then I get the flag
    And I solve the challenge
