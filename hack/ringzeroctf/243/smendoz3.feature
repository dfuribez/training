## Version 2.0
## language: en

Feature: Who I am part 2 - Forensics - RingZer0
  Site:
    https://ringzer0ctf.com
  Category:
    Forensics
  User:
    arguoK
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given Challenge title:
    """
    Who I am part 2
    """
    And A encrypted code
    """
     I'm the proud owner of this website. Can you verify that?
    """

  Scenario: Fail: Page code comments
    Given The challenge webpage
    When I use developer tools to inspect web elements
    """
    <!--
    Always believe in yourself <3
    4a4668222a53531a593a022f7332354a2c36771c070b3343045b2658570e3c5f60
    77393266762206283f62183d49397c3f703d60667b0a2f6a72745b092466116a41
    3d75372752710c2a2621331b304252407740644a4655366a08735e323c7f3c6f
    -->
    """
    Then I can see a comment
    When I organize it in a hexdump format and try to unhex it
    """
    4a46 6822 2a53 531a 593a 022f 7332 354a 2c36 771c 070b 3343 045b 2658 570e
    3c5f 6077 3932 6676 2206 283f 6218 3d49 397c 3f70 3d60 667b 0a2f 6a72 745b
    0924 6611 6a41 3d75 3727 5271 0c2a 2621 331b 3042 5240 7740 644a 4655 366a
    0873 5e32 3c7f 3c6f
    """
    Then I get no useful information
    And I fail to find the flag

  Scenario: Success: PGP search the webpage owner
    Given Challenge webpage
    And Author name "MrUn1k0d3r"
    When I search in google "PGP MrUn1k0d3r"
    Then I can see a webpage with the Keybase for PGP:
    """
    -----BEGIN PGP MESSAGE-----
    Version: Keybase OpenPGP v2.0.55
    Comment: https://keybase.io/crypto

    Base64 KEY
    -----END PGP MESSAGE-----

    And finally, I am proving ownership of this host by posting or
    appending to this document.
    View my publicly-auditable identity here: https://keybase.io/mrun1k0d3r

    ==================================================================

    FLAG-XXXXXX
    """
    And I can see the flag format
    And I captured the flag
