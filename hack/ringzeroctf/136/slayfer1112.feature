## Version 2.0
## language: en

Feature: CHALLENGE-Ringzer0ctf
  Site:
    Ringzer0ctf
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag passing the login form.

  Background:
  Hacker's software:
    | <Software name>  | <Version>     |
    | Windows OS       | 10            |
    | Chrome           | 79.0.3945.130 |
    | Shazam           | 10.26.0       |
    | Sonic Visualiser | 4.0.1         |
    | Photoshop        | CS6           |
  Machine information:
    Given I am accessing to the website
    Then I choose a challenge
    And I was redirect to a login form

  Scenario: Fail:Type-the-song-name
    Given I'm on the challenge page
    And The challenge asks me that download a file
    When I download the file I see that the file has a ".ogg" extension
    Then I search about that extension and I that is for an audio files
    And I reproduce the audio
    When I reproduce the audio I heard a song
    Then I think that this song can be a hint for the flag
    And I try to find the name of the song
    When I was trying to find the name I think that I can use the app "Shazam"
    Then I reproduce the song and analyze with "Shazam"
    And I get the name "Stack It Up"
    When I know the name of the song
    And I try to use it how the flag
    Then I receive a message that says "Wrong flag; try harder!"
    And I fail to get the flag

  Scenario: Success:Inspect-code-to-see-the-script
    Given I'm on the challenge page
    And The song have any hint
    When I reproduce the song in youtube
    Then I compare it with the file that I download
    And I think that I can hear a weird sounds
    When I think on that the sounds are weird but similar to the original song
    Then I think that the sounds can be the clue that I was seeking
    And I open "Sonic Visualiser"
    When I'm on Sonic visualiser I open the file
    Then I see the frequency waves and looks normal
    And I search on internet about song files and hidden messages
    When I read some about that I know that some people send message in songs
    Then I see that the messages are sent modifying the song
    And I try to see the spectrogram of this song
    When I see the spectrogram I notice that has weird things
    And I use the scroll to accommodate the size to see better
    When I fix the spectrogram to a good size [evidence](img1)
    Then I see a hint that says "Close to the solution"
    And I see a QR code
    When I try to scan the WR was to difficult for the interference and color
    Then I try to make a more clear image with Photoshop
    And I get a clear QR code [evidence](img2)
    When I need to decode the QR
    Then I try to use this website "https://zxing.org/w/decode.jspx"
    And I upload my QR
    When I submit my QR and wait for the response
    Then I get the decoded message with the flag [evidence](img3)
    And I try to use this flag in the submit form in the challenge
    When I paste the flag in the form
    Then I submit and see a congrats message
    And I found the flag
