## Version 2.0
## language: en

Feature: overthewire.org
  Site:
    http://overthewire.org/wargames/bandit/
  User:
    charlie517 (wechall)
  Goal:
    Get access into the server.

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Linux Mint      | 4.15.0-45     |
    | SSH             | OpenSSH_7.6p1 |
  Machine information:
    Given I am accessing the bandit.labs.overthewire.org
    And SSH with ssh bandit.labs.overthewire.org -p 2220
    And enter a console that allows me to move around the system
    And SSH version OpenSSH_7.4p1
    And is running on Linux bandit with kernel 4.18.12

  Scenario: Success:
    Given I am logged in the machine
    And I print readme
    Then I can see the password for level1
    And get access to bandit1 [bandit0-evidence](bandit0.png)
