let
  pkgs = import ../pkgs/stable.nix;
  builders.pythonPackage = import ../builders/python-package pkgs;
in
  pkgs.stdenv.mkDerivation (
        (import ../src/basic.nix)
    //  (import ../src/external.nix pkgs)
    //  (rec {
          name = "builder";

          buildInputs = [
            pkgs.git
            pkgs.curl
            pkgs.cacert
            pkgs.nodejs
            pkgs.bats
            pkgs.python37
          ];

          pyPkgPyparsing = builders.pythonPackage "pyparsing==2.2.2";
        })
  )
