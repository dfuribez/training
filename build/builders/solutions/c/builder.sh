source "${srcGeneric}"

function lint {
  local solution="${1}"

  cppcheck \
    --error-exitcode=1 \
    "${solution}"
}

function compile {
  local solution="${1}"

  gcc \
    -Werror \
    "${solution}" \
    -o "${solution%.*}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
