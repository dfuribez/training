source "${srcGeneric}"

function lint {
  local solution="${1}"
  local solution_lint_log
  local regex_linter='(ERROR|WARN|Checkstyle ends with [0-9]* errors.)'

      solution_lint_log=$( \
        java \
          -jar "${srcJavaCheckstyle}" \
          -c "${srcJavaCheckstyleCofig}" \
          "${solution}" \
          2>&1 \
      ) || true \
  &&  if echo "${solution_lint_log}" | grep -P "${regex_linter}"
      then
        return 1
      fi
}

function compile {
  local solution="${1}"

  javac "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
