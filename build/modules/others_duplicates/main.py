#!/usr/bin/env python3

"""
This script checks duplicated urls in OTHERS.lst files
"""

import os
import sys
from urllib.parse import urlparse


def canonicalize_if_youtube_url(url):
    """If the provided url is from YouTube, return the video ID."""
    if 'www.youtube.com' in url:
        parsed_url = urlparse(url)
        return parsed_url[4][2:]
    if 'youtu.be' in url:
        parsed_url = urlparse(url)
        return parsed_url[2][1:]
    return url


def normalize_others_file(file_path):
    """Extract urls from a OTHERS.lst file, filter them, and return them."""
    with open(file_path) as file_handle:
        urls = file_handle.read().splitlines()
        return tuple(canonicalize_if_youtube_url(url) for url in urls if url)


def yield_others(starting_folder):
    """Recursively yield paths to OTHERS.lst files starting from folder."""
    for dirpath, _, filenames in os.walk(starting_folder):
        for filename in filter('OTHERS.lst'.__eq__, filenames):
            file_path = os.path.join(dirpath, filename)
            yield file_path


def check_for_duplicated_urls(starting_folder):
    """Parse every OTHER.lst file and check if it has duplicated urls."""
    for others_file_path in yield_others(starting_folder):
        normalized_urls = normalize_others_file(others_file_path)
        unique_normalized_urls = set(normalized_urls)
        if sorted(normalized_urls) != sorted(unique_normalized_urls):
            print('Files with duplicated urls:')
            print('  ', others_file_path)
            return True
    return False


def main():
    """Usual entrypoint."""
    for folder in ('code', 'hack'):
        if check_for_duplicated_urls(folder):
            sys.exit(1)
    sys.exit(0)


if __name__ == '__main__':
    main()
