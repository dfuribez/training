// $ cppcheck sebasoviedo69 #linting
// $ g++ sebasoviedo69.cpp -o sebasoviedo69 #compilation

#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <vector>
#include <fstream>

// world's constans
const int hill_blocks{40};
const int shoot_attemps{3};
const int block_size{4};
const int max_time{60};
const double pi{3.1416};
const double gravity{9.81};
// Hill's slope
std::vector<double> slope{};
// velocity and angle three times
std::vector<std::vector<double>> initial_conditions{};

void initialization() {
  for(int i = 0; i < 40; i++){
    slope.push_back(0);
  }
}

double projectile_motion(double velocity_0, double angle) {
  angle = angle * pi / 180.0; // Converts the angle to radians
  bool collision{false};
  double time{0.01};
  double distance_x{0.0};
  double distance_y{0.0};
  double collision_at{0.0};
  do{ // Equations of projectile motion
  distance_x = velocity_0 * cos(angle) * time;
  distance_y = velocity_0 * sin(angle) * time;
  distance_y -= (0.5 * gravity * pow(time, 2));
  // Calculing the collision
  if(distance_x < static_cast<int>(slope.size())
  *block_size && distance_y >= 0) {
    if(floor(distance_y) < slope.at(floor(distance_x / block_size))){
      collision_at = distance_x;
      collision = true;
    }
  }else if(time == max_time) {
    std::cout << " There was no collision." << std::endl;
  }
    time += 0.01;
  } while ((collision == false) && (time < max_time));
  return collision_at;
}

int main() {
  double angle{0.0};
  double velocity_0{0.0};
  double collision_at{0.0};
  std::vector<double> data{};
  std::ifstream readfile;
  readfile.open("./DATA.lst");
  char output[300];
  if(readfile.is_open()) {
    while (!readfile.eof()) {
      readfile >> output;
      data.push_back(atoi(output));
    }
  }
  readfile.close();
  initialization();

  int step {0};
  int count{0};
  for(int i = 0; i < shoot_attemps; i++) {
    count = 0;
    for(int j = step; j < step+hill_blocks+6; j++) {
      if(count < hill_blocks) {
        slope.at(count) = data.at(j)*block_size;
      } else {
        velocity_0 = data.at(j);
        angle = data.at(j+1);
        collision_at = projectile_motion(velocity_0, angle);
        std::cout << floor(collision_at) << " ";
        j++;
        count++;
      }
      count++;
    }
    step += hill_blocks+6;
  }
  std::cout << std::endl;
  return 0;
}

/*
$ ./sebasoviedo69
96 68 120 60 88 120 97 68 76
*/

