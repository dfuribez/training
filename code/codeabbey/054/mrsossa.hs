{-
 $ ghc  mrsossa.hs
   [1 of 1] Compiling Main ( mrsossa.hs )
   Linking code ...
 $ hlint mrsossa.hs
   No hints
-}

import System.IO
import Control.Monad

processfile :: Handle -> IO()
processfile ifile =
  do
    iseof <- hIsEOF ifile
    Control.Monad.unless iseof $
      do
        line <- hGetLine ifile
        let numbers = map read $ words line :: [Int]
        let num = head numbers `div` 2
        let res c1 c2
              | c2 * c2 + c2 * c1 < num = res c1 (c2+1)
              | c2 * c2 + c2 * c1 == num = show ((c1 * c1 + c2 * c2) ^ 2) ++ ""
              | otherwise = res (c1+1) 1
        let output = res 1 2
        print output
        processfile ifile

main =
  do
    ifile <- openFile "DATA.lst" ReadMode
    line <- hGetLine ifile
    processfile ifile
    hClose ifile

{-
 $ ./mrsossa
 88390026150409 41051455793881 42074487655225 107016728422321
100589526301489 65692078654225 31933179393025
-}
