/*
$ golint juansorduz97.go
$ go run juansorduz97.go
*/
package main
import(
  "fmt"
  "bufio"
  "os"
  "strconv"
)
func main() {
  file,_ := os.Open("DATA.lst")
  defer file.Close()
  InputBuffer := bufio.NewScanner(file)
  InputBuffer.Scan()
  FirstInput,_ := strconv.ParseInt(InputBuffer.Text(),10,8)
  NumInputs := int(FirstInput)
  var Output = []byte{0}
  for i := 0; i < NumInputs; i++ {
    CheckValue := false
    InputBuffer.Scan()
    Line := InputBuffer.Text()
    KCP := int(Line[0])
    KRP := int(Line[1])
    QCP := int(Line[3])
    QRP := int(Line[4])
    if KRP == QRP && CheckValue == false {
      Output = append(Output,1)
      CheckValue = true
    }
    if KCP == QCP && CheckValue == false {
      Output = append(Output,1)
      CheckValue = true
    }
    if (KCP-QCP)*(KCP-QCP)==(KRP-QRP)*(KRP-QRP) && CheckValue == false {
      Output = append(Output,1)
      CheckValue = true
    }
    if CheckValue == false {
      Output = append(Output,0)
    }
  }
  for i := 1; i < len(Output); i++ {
    CheckValuePrint := false
    if Output[i] == 0 && CheckValuePrint == false {
      fmt.Print("N ")
      CheckValuePrint = true
    }
    if Output[i] == 1 && CheckValuePrint == false {
      fmt.Print("Y ")
      CheckValuePrint = true
    }
    if CheckValuePrint == false {
      fmt.Print("Unexpected Error")
    }
  }
  fmt.Println("")
}
/*
$ go run juansorduz97.go
N N N N N N N N N N N N Y N Y N Y N N N Y N N N Y
*/
