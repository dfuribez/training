/**
> golint slayfer1112.go
**/

package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

func main() {

  dictionary := [...]string{" ", "e", "t", "o", "n", "a", "s", "i", "r", "h",
    "d", "l", "!", "u", "c", "f", "m", "p", "g", "w", "b", "y", "v", "j", "k",
    "x", "q", "z"}

  bytes := [...]string{"11", "101", "1001", "10001", "10000", "011", "0101",
    "01001", "01000", "0011", "00101", "001001", "001000", "00011", "000101",
    "000100", "000011", "0000101", "0000100", "0000011", "0000010", "0000001",
    "00000001", "000000001", "0000000001", "00000000001", "000000000001",
    "000000000000"}
  var data []string
  var join string
  var bits []string
  var array string
  var hex []string

  dataIn, _ := os.Open("DATA.lst")

  dataScan := bufio.NewScanner(dataIn)

  for dataScan.Scan() {
    line := strings.Split(dataScan.Text(), "")
    data = line
  }

  for i := 0; i < len(data); i++ {
    for j := 0; j < len(dictionary); j++ {
      if data[i] == dictionary[j] {
        data[i] = bytes[j]
      }
    }
  }

  for i := 0; i < len(data); i++ {
    join += data[i]
  }

  data = strings.Split(join, "")
  for (len(data) % 8) != 0 {
    data = append(data, "0")
  }

  for i := 1; i <= len(data); i++ {
    array += data[i-1]
    if ((i % 8) == 0) && (i != 0) {
      bits = append(bits, array)
      array = ""
    }
  }

  for i := 0; i < len(bits); i++ {
    hexa, _ := strconv.ParseInt(bits[i], 2, 64)
    hexaconv := strings.ToUpper(strconv.FormatInt(hexa, 16))
    hex = append(hex, hexaconv)
    if len(hex[i]) < 2 {
      hex[i] = "0" + hex[i]
    }
    fmt.Print(hex[i] + " ")
  }
}

/**
> go run slayfer1112.go
A0 29 25 76 8B A8 C4 12 A1 82 26 F2 6E 1C C7 42 42 1C F2 74 35 A4 80 D5 C1 01
D8 28 92 95 34 C0 27 27 71 22 80 D7 05 6C 30 6E 12 98 04 D3 0D CC D8 4A 63 05
E6 3C 46 8C 80 48 4C A5 4F 04 8B 26 8B 0C 80 77 36 03 70 6D 10 A5 E4 E8 78 88
88 7B 81 70 A8 AA A9 80 4D 21 85 8A 25 C0 5B 8E 04 D5 5D 46 A1 6C 0B 4B A6 1C
9D 4A 38 85 A8 72 98 C3 94 92 4E 69 5C 86 AB 61 20
**/
