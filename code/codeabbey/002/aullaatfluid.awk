#!/usr/bin/env awk
# linting
# $ awk -L -f aulloaatfluid.awk
# 31165

BEGIN{
    A = 0
    temp = 0
    getline A < "DATA.lst"
    getline B < "DATA.lst"
    split (B,C," ")
    for (i=1; i<A; i++)
      temp = temp + C[i]
      print temp
    close("DATA.lst")
}

# $ awk -f aulloaatfluid.awk DATA.lst
# 31165
