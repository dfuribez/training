#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 5.98 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Int32)
  data.each_line do |x|
    inter = [] of Int32
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_i : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  day1 = array[0]
  hour1 = array[1]
  min1 = array[2]
  sec1 = array[3]
  total1 = (day1*24*60*60)+(hour1*60*60)+(min1*60)+sec1

  day2 = array[4]
  hour2 = array[5]
  min2 = array[6]
  sec2 = array[7]
  total2 = (day2*24*60*60)+(hour2*60*60)+(min2*60)+sec2

  total = total2-total1

  mod = total.divmod(60)
  sec = mod[1]
  mod = mod[0].divmod(60)
  min = mod[1]
  mod = mod[0].divmod(24)
  hour = mod[1]
  day = mod[0]

  print "(#{day} #{hour} #{min} #{sec}) "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# (7 3 19 53) (14 1 20 35) (1 14 51 46) (5 13 29 48) (6 7 14 23) (8 10 39 2)
# (22 22 28 10) (15 15 22 53) (2 4 33 17) (2 16 25 45) (11 7 6 7) (19 22 19 46)
