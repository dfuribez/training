// $ cppcheck oscardjuribe.cpp
// Checking 2048.cpp ...

//code to solve 2048 problem from code abbey

#include <iostream>
#include <string>

#define ROWS 4
#define COLS 4

using namespace std;

void empty_spaces (int matrix[ROWS][COLS], int row,int col, int type) {
  //Method to move the tiles where are empty spaces

  switch(type) {
  // Down
    case 1:
      // iterate over array
      for (int i = 0; i<ROWS ; i++) {
        // find zeros in column
        if (matrix[i][col]==0) {
          // down the tiles that can down
          for (int j = i; j >0; j--) {
            matrix[j][col]=matrix[j-1][col];
            matrix[j-1][col]=0;
          }
        }
      }
      break;
    // Up
    case 2:
      // iterate over array
      for (int i = ROWS-1; i >= 0; i--) {
        // find zeros in column
        if (matrix[i][col]==0) {
          for (int j = i; j<ROWS-1; j++) {
            matrix[j][col]=matrix[j+1][col];
            matrix[j+1][col]=0;
          }
        }
      }
      break;
    //Left
    case 3:
      // iterate over array
      for (int i = COLS-1; i >= 0; i--) {
        // find zeros in rows
        if (matrix[row][i]==0) {
          // move the tiles that can go to left
          for (int j = i; j < COLS-1; j++) {
            matrix[row][j]=matrix[row][j+1];
            matrix[row][j+1]=0;
          }
        }
      }
      break;
    // Right
    case 4:
      // iterate over array
      for (int i = 0; i <COLS ; i++) {
        // find zeros in rows
        if (matrix[row][i]==0) {
          // move the tiles that can go to right
          for (int j = i; j >0 ; j--) {
            matrix[row][j]=matrix[row][j-1];
            matrix[row][j-1]=0;
          }
        }
      }
      break;
  }
}

void down (int matrix[ROWS][COLS]) {
  // Method to simulate the down movement

  // iterate over all columns
  for (int j = 0; j < COLS; j++) {
    // iterate over rows starting at the last one
    for (int i = ROWS-1; i>0; i--) {
      // if current pos is equals to the next pos then add them
      if (matrix[i][j]==matrix[i-1][j] && matrix[i][j]!=0) {
        // add values
        matrix[i][j]+=matrix[i-1][j];
        // make zero current value
        matrix[i-1][j]=0;
        // move one row position behind
        i-=1;
      }
    }
    // move the tiles down
    empty_spaces(matrix,0,j,1);
  }
}

void up (int matrix[ROWS][COLS]) {
  // Method to simulate the up movement

  // iterate over all columns
  for (int j = 0; j < COLS; j++) {
    // iterate over rows starting at the first one
    for (int i = 0; i<ROWS-1; i++) {
      // if current pos is equals to the next pos then add them
      if (matrix[i][j]==matrix[i+1][j] && matrix[i][j]!=0) {
        // add values
        matrix[i][j]+=matrix[i+1][j];
        // make zero value
        matrix[i+1][j]=0;
        // move one row position ahead
        i+=1;
      }
    }
    // move the tiles up
    empty_spaces(matrix,0,j,2);
  }
}

void left (int matrix[ROWS][COLS]) {
  // Method to simulate the left movement

  // iterate over all rows
  for (int i = 0; i <ROWS; i++) {
    // iterate over cols starting at the last one
    for (int j =0; j<COLS-1; j++) {
      // if current pos is equals to the next pos then add them
      if (matrix[i][j]==matrix[i][j+1] && matrix[i][j]!=0) {
        // add values
        matrix[i][j]+=matrix[i][j+1];
        // make zero
        matrix[i][j+1]=0;
        // move one cols position ahead
        j+=1;
      }
    }
    // move tiles to the left
    empty_spaces(matrix,i,0,3);
  }
}

void right (int matrix[ROWS][COLS]) {
  // Method to simulate the right movement
  // iterate over all rows
  for (int i = 0; i <ROWS ; i++) {
    // iterate over cols starting at the last one
    for (int j = COLS-1; j>0; j--) {
      // if current pos is equals to the next pos then add them
      if (matrix[i][j]==matrix[i][j-1] && matrix[i][j]!=0) {
        // add values
        matrix[i][j]+=matrix[i][j-1];
        // make zero
        matrix[i][j-1]=0;
        // move one cols position behind
        j-=1;
      }
    }
    //move the tiles to the right
    empty_spaces(matrix,i,0,4);
  }
}

int count_values (int matrix[ROWS][COLS],int value) {
  // Method to calculate number of repetitions
  // counter
  int cont=0;
  // iterate over matrix searching the value passed as parameter
  for (int i = 0; i < ROWS; i++) {
    for (int j = 0; j < COLS; j++) {
      if (matrix[i][j]==value) {
        cont++;
      }
    }
  }

  return cont;
}

int spare_zeros (int results[]) {
  // Method to search final zeros

  // counter
  int pos=10;

  // search until what position are trailing zeros
  while (results[pos]==0) {
    pos--;
  }

  return pos;
}

void calculate_results (int matrix[ROWS][COLS]) {
  // Method to calcualte the results and print them

  // contain the number of values at each position
  // each position is a power of 2, like this
  // 2 4 8 16 32 64 128 256 512 1024 2048
  // 0 1 2 3  4  5  6   7   8   9    10
  int results[11]={0,0,0,0,0,0,0,0,0,0,0};
  //value to search power of 2
  int value=2;
  for (int i = 0; i < 11; i++) {
    // calculate how many numbers of each value
    results[i]=count_values(matrix,value);
    // 2 pow n
    value*=2;
  }
  // get the postion from which all the other numbers are zeros
  int final_pos=spare_zeros(results);

  for (int i = 0; i <= final_pos; ++i) {
    cout<<results[i]<<" ";
  }

  cout<<endl;
}

int main () {

  // matrix containing the board
  int matrix[ROWS][COLS];

  // fill the matrix from stdin
  for (int i = 0; i < ROWS; i++) {
    for (int j = 0; j < COLS; j++) {
      // read single int
      cin>>matrix[i][j];
    }
  }
  // string containing the movements
  string str_movements;
  // read empty line
  std::getline(cin, str_movements);
  // read movements
  std::getline(cin, str_movements);

  // iterate over the chars in the string but avoiding spaces
  for (int i = 0; i < str_movements.size(); i+=2) {
    // if char is D then move down
    if (str_movements[i]=='D') {
      down(matrix);
      }
    // if char is U then move up
    else if(str_movements[i]=='U') {
      up(matrix);
    }
    // if char is L then move left
    else if(str_movements[i]=='L') {
      left(matrix);
    }
    // if char is R then move right
    else if (str_movements[i]=='R') {
      right(matrix);
    }
  }
  // calculate the repetition of values from the matrix
  calculate_results(matrix);

  return 1;
}
