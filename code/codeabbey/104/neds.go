/*
$ gofmt -w neds.go
$ go vet neds.go
vet.exe: errors: 0; warnings: 0
$ go build -work neds.go
WORK=C:\Users\NEDS\AppData\Local\Temp\go-build957807318
*/

package main

import (
  "bufio"
  "fmt"
  "log"
  "math"
  "os"
  "strconv"
  "strings"
)

func convString(el string) int {
  number, e := strconv.Atoi(el)
  if e != nil {
    fmt.Println(e)
  }
  return number
}

func readData() (int, []int) {
  var data []string
  var vertex []int
  file, err := os.Open("DATA.lst")
  if err != nil {
    log.Fatal(err)
  }
  defer file.Close()

  lines := bufio.NewScanner(file)
  for lines.Scan() {
    data = append(data, lines.Text())
  }

  if err := lines.Err(); err != nil {
    log.Fatal(err)
  }
  n := convString(data[0])
  for i := 1; i < len(data); i++ {
    aux := strings.Split(data[i], " ")
    for j := 0; j < len(aux); j++ {
      vertex = append(vertex, convString(aux[j]))
    }
  }
  return n, vertex
}

func main() {
  n, vert := readData()
  var out string
  for i := 0; i < n; i++ {
    sideXa := math.Abs(float64(vert[i*6] - vert[i*6+2]))
    sideXb := math.Abs(float64(vert[i*6+1] - vert[i*6+3]))
    sideYa := math.Abs(float64(vert[i*6] - vert[i*6+4]))
    sideYb := math.Abs(float64(vert[i*6+1] - vert[i*6+5]))
    sideZa := math.Abs(float64(vert[i*6+2] - vert[i*6+4]))
    sideZb := math.Abs(float64(vert[i*6+3] - vert[i*6+5]))
    a := math.Sqrt(sideXa*sideXa + sideXb*sideXb)
    b := math.Sqrt(sideYa*sideYa + sideYb*sideYb)
    c := math.Sqrt(sideZa*sideZa + sideZb*sideZb)
    s := (a + b + c) / 2
    area := math.Sqrt(s * (s - a) * (s - b) * (s - c))
    area = math.Round(area*10) / 10
    out += fmt.Sprintf("%.1f", area) + " "
  }
  fmt.Println(out)
}

/*
$ neds.exe
8008545.5 9597811.0 11915667.0 11450171.0 15066084.0
2427172.5 3265797.0 31022920.0 492795.0 5095621.0
4526802.0 8112862.5 1371713.0 1036641.0 7015461.5
20091288.5
*/
