/*
dscanner.exe --styleCheck pperez7.d
dscanner.exe --syntaxCheck pperez7.d
dscanner.exe --report pperez7.d
"issues": [];
*/

import std.stdio;
import std.file;
import std.array;
import std.conv;
import std.math;
import std.format;

void main(){
  File file = File("DATA.lst", "r");
  string testcases = file.readln();
  const int triangles = parse!int(testcases);
  double [] answers = new double[](triangles);

  for(int i = 0; i < triangles; i++){
    string input = file.readln();
    string [] sides = input.split(" ");
    int [] X;
    int [] Y;
    for(int j = 0; j < 6; j++){
      if(j % 2 == 0){
        X ~= parse!int(sides[j]);
      }
      else{
        Y ~= parse!int(sides[j]);
      }
    }
    double A = 0;
    for(int j = 0; j < 3; j++){
      if(j+1 > 2){
        A = A + X[j]*Y[0];
        A = A - Y[j]*X[0];
      }
      else{
        A = A + X[j]*Y[j+1];
        A = A - Y[j]*X[j+1];
      }
    }
    answers[i]=abs(A/2);
  }
  writefln("%-(%.10g %)",answers[]);
}

/*
dmd -run pperez7.d
8008545.5 9597811 11915667 11450171 15066084 2427172.5 3265797 31022920
492795 5095621 4526802 8112862.5 1371713 1036641 7015461.5 20091288.5
*/
