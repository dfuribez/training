;; $ clj-kondo --lint andresclm.clj
;; linting took 16ms, errors: 0, warnings: 0

(ns andresclm
  (:require [clojure.string :as str]))

(defn get-range [n]
  (->> n inc (range 1) vec))

(defn get-data-from [file]
  (let [data (slurp file)
        data' (str/split data #"\n")]
    (vec (str/split (first (rest data')) #" "))))

(defn avg [x y z]
  (/ (+ x y z)
     3))

(defn -main "Entry point" []
  (let [data (get-data-from "DATA.lst")
        my-sequence (get-range (- (count data) 2))
        temp (map (fn [position] (let [[x y z]  (subvec data
                                                        (- position 1)
                                                        (+ position 2))
                                       result (avg (Float/parseFloat x)
                                                   (Float/parseFloat y)
                                                   (Float/parseFloat z))]
                                   (format "%.7f" result)))
                  my-sequence)
        temp' (conj temp (first data))
        results (conj (vec temp') (last data))]
    (apply print results)))

;; $ clj -m andresclm
;; 32.6 33.0000000 34.6000010 39.1666679 41.4666672 43.6999995 44.1
