--[[
$ ./luacheck.exe anders2d.lua
Checking anders2d.lua                             OK
Total: 0 warnings / 0 errors in 1 file
]]
require 'io'


local nTestCases
local dataArr
local data
dataArr = {}
data = io.lines("DATA.lst")
nTestCases=0

local function split(s, delimiter)
    local splitted = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(splitted, match);
    end
    return splitted;
end


local function check(count1, count2, array)
    if count1 == array[1] and count2 == array[2] then return true end
    return false
end

local function checkSequence(count1, count2, array)
    local sum
    sum=0
    for i=count1, count2 do
        sum=sum+array[i]
    end

    if sum == 5 then
        return true
    end

end

local function main()
    for line in data do
        if nTestCases==0  then
            nTestCases=line
        else

            local lineSplitted;
            lineSplitted=split(line," ")

             table.insert(dataArr, lineSplitted)

        end
    end
    for i=1,nTestCases do
    local dicePos
        local c = {0,0,0,0,0,0}
        local dice = dataArr[i]
        local count = {0,0}


        for posIterator=1,5,1 do
            dicePos = tonumber(dice[posIterator])
            c[dicePos]=c[dicePos]+1
        end

        for cIterator=1,6 do
            if c[cIterator]>=2 then
                if count[1]==0 then
                    count[1]=c[cIterator]
                else
                    count[2]=c[cIterator]
                    break
                end
            end
        end

        table.sort(count)
        if check(0,2,count) then
            io.write("pair ")
        elseif check(0,3,count) then
            io.write("three ")
        elseif check(0,4,count)then
            io.write("four ")
        elseif check(0,5,count)then
            io.write("yacht ")
        elseif check(2,2,count)then
            io.write("two-pairs ")
        elseif check(2,3,count)then
            io.write("full-house ")
        elseif checkSequence(1,5,c) then
            io.write("small-straight ")
        elseif checkSequence(2,6,c) then
            io.write("big-straight ")
        else
            io.write("none ")
        end
    end
end

main()
--[[
$ lua -e "io.stdout:setvbuf 'no'" "anders2d.lua"
four pair pair small-straight small-straight small-straight yacht
small-straight small-straight big-straight pair pair two-pairs small-straight
two-pairs pair pair big-straight small-straight two-pairs pair two-pairs
small-straight two-pairs two-pairs pair pair
]]
