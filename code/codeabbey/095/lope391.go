/*
$ gofmt -w lope391.go
$ go vet lope391.go
vet.exe: errors: 0; warnings: 0
$ go build -work lope391.go
WORK=/tmp/go-build893807118
*/

package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func main() {

	absPath, _ := filepath.Abs("DATA.lst")
	arr, _ := readFileToStr(absPath)
	bArr := strings.Split(arr[0], " ")
	b1, _ := strconv.Atoi(bArr[0])
	b2, _ := strconv.Atoi(bArr[1])
	arr = arr[1:]

	var xVals []float64
	var yVals []float64

	cont1 := 0.0
	cont2 := 0.0
	for i := 0; i <= (b2 - b1); i++ {
		vals := strings.Split(arr[i], " ")
		rain, _ := strconv.ParseFloat(vals[1], 64)
		price, _ := strconv.ParseFloat(vals[2], 64)

		cont1 += rain
		xVals = append(xVals, rain)
		cont2 += price
		yVals = append(yVals, price)
	}

	xAv := cont1 / float64(len(xVals))
	yAv := cont2 / float64(len(yVals))

	cont1 = 0.0
	cont2 = 0.0
	for i := 0; i <= (b2 - b1); i++ {
		cont1 += math.Pow(xVals[i]-xAv, 2) * ((yVals[i] - yAv) / (xVals[i] - xAv))
		cont2 += math.Pow(xVals[i]-xAv, 2)
	}

	b := cont1 / cont2
	a := yAv - (b * xAv)

	fmt.Printf("%v %v\n", b, a)
}

func readFileToStr(fn string) (arr []string, err error) {

	//Save file to buffer and close
	file, err := os.Open(fn)
	defer file.Close()

	if err != nil {
		return nil, err
	}

	//Cread reader buffer from file
	reader := bufio.NewReader(file)

	//Start reading buffer
	var line string
	for {
		line, err = reader.ReadString('\n')
		//Delete \n from last character of string
		if last := len(line) - 1; last >= 0 && line[last] == '\n' {
			line = line[:last]
		}
		if line != "" {
			arr = append(arr, line)
		}
		if err != nil {
			break
		}
	}

	if err != io.EOF {
		fmt.Printf("ERROR: %v\n", err)
	}

	return arr, nil
}

/*
./lope391
2.0985072746740565 92.1100937414709
*/
