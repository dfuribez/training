<?php
/*
 >php-lint jdiegoc.php
 No syntax errors detected in jdiegoc.php
*/

$N = readline();
$str = readline();
$a = explode(" ", $str);
$input = "";
for ($i = 0; $i < $N; $i++) {
  $j = 0;
  while ($a[$i] != 1) {
    if ($a[$i] % 2 == 0)
      $a[$i] = $a[$i] / 2;
    else $a[$i] = $a[$i] * 3 + 1;
     $j++;
    }
   $input .= $j.' ';
  }
echo $input;

/*
 $ php jdiegoc.php
 106 9 24 44 11 104 22 73 64 29 10 26 45 40 20 111 187 126 23 70 11 45 21
*/
?>
