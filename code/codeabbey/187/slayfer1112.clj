;; $ clj-kondo --lint slayfer1112.clj
;; linting took 16ms, errors: 0, warnings: 0

(ns slayfer1112-038
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.java.shell :as shell]))

(defn get-data [file]
  (let [dat (slurp file)
        datv (str/split-lines dat)
        head (str/split (datv 0) #" ")]
    [head]))

(defn solution [head]
  (let [web (str "Secret value is " head)
        folder "/Codeabbey"
        challenge "/187"
        filename "/static-web-page.txt"
        path (str folder challenge filename)
        git (str "cd ./Codeabbey; git ")]
    (spit (str "." path) web)
    (println (:out (shell/sh "sh" "-c" (str git "add ."))))
    (println (:out (shell/sh "sh" "-c" (str git "commit -m 'Secret'"))))
    (println (:out (shell/sh "sh" "-c" (str git "push"))))
    (println (str "https://pastelito02.github.io" path)))
  (Thread/sleep 5000)
  (System/exit 0))

(defn main []
  (let [[head] (get-data "DATA.lst")]
    (solution (head 0))))

(main)

;; https://pastelito02.github.io/Codeabbey/187/static-web-page.txt
