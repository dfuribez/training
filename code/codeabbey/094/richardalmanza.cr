#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split("\n") if fluid
args = args[0].split("\n") if !fluid

args.delete_at(0)

args.each do |nums|
  list = nums.split.map {|x| x.to_i ** 2}

  print "#{list.sum} "
end

puts

# $ ./richardalmanza.cr
# 93 224 1760 219 1791 179 973 583 311 568 26 276 37 924 322 1090 1192 0
