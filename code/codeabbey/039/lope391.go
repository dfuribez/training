/*
$ gofmt -w lope391.go
$ go vet lope391.go
vet.exe: errors: 0; warnings: 0
$ go build -work lope391.go
WORK=/tmp/go-build596963989
*/
package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func main() {

	absPath, _ := filepath.Abs("DATA.lst")
	arr, _ := readFileToStr(absPath)
	step, _ := strconv.Atoi(arr[0])
	for i := 1; i <= step; i++ {
		a := strings.Split(arr[i], " ")
		stDEV, mean, tag := getSTDDev(a)
		limit := 0.01 * mean
		if stDEV > 4*limit {
			fmt.Println(tag)
		}

	}
}

func readFileToStr(fn string) (arr []string, err error) {

	//Save file to buffer and close
	file, err := os.Open(fn)
	defer file.Close()

	if err != nil {
		return nil, err
	}

	//Cread reader buffer from file
	reader := bufio.NewReader(file)

	//Start reading buffer
	var line string
	for {
		line, err = reader.ReadString('\n')
		//Delete \n from last character of string
		if last := len(line) - 1; last >= 0 && line[last] == '\n' {
			line = line[:last]
		}
		if line != "" {
			arr = append(arr, line)
		}
		if err != nil {
			break
		}
	}

	if err != io.EOF {
		fmt.Printf("ERROR: %v\n", err)
	}

	return arr, nil
}

func getSTDDev(arr []string) (stDEV float64, mean float64, tag string) {

	tag = arr[0]
	sum := 0
	//Find mean
	for i := 1; i < len(arr); i++ {
		val, _ := strconv.Atoi(arr[i])
		sum += val
	}
	mean = float64(sum) / float64(len(arr)-1)
	//Find st. Dev.
	sum2 := 0.0
	for i := 1; i < len(arr); i++ {
		val, _ := strconv.Atoi(arr[i])
		sum2 += math.Pow(mean-(float64(val)), 2)
	}
	stDEV = math.Sqrt(sum2 / float64(len(arr)-1))
	return stDEV, mean, tag
}

/*
./lope391
GOLD
DALG
BLEP
OBAM
FANT
FLNT
JOOG
ASDF
CKCL
MYTH
WOWY
INSX
IMIX
SUGR
SAKK
ZEOD
GEEK
JABA
MARU
PNSN
SLVR
JEOP
VDKL
*/
