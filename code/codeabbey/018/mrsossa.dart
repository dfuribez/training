/**
 * $ dartanalyzer mrsossa.dart
 * Analyzing mrsossa.dart
 * No issues found!
 */

import 'dart:io';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    var c = int.parse(data[0]);
    for (var i = 1; i <= c; i++) {
      var cas = data[i];
      var cas2 = cas.split(" ");
      var x = int.parse(cas2[0]);
      var n = int.parse(cas2[1]);
      var r = 1.0;
      for (var i = 0; i < n; i++) {
        r = (r+(x/r))/2;
      }
      print(r);
    }
  });
}

/* $ dart mrsossa.dart
7.416198487095663 6.08276253029822 22.068076490713914 293.5188235491771
5.47722557564769 81.15417426109393 59.67411499134277 89.70507232035433
2178.9583211890717 27.53179979587241 12.755078162170646 9.110433579154478
4.898979485566356
*/
