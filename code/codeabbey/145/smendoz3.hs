-- $ ghc -o smendoz3 smendoz3.hs
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

import Control.Monad

main = do
  input <- getLine
  inputs <- replicateM (read input) getLine
  print (unwords (checkAll inputs))

checkAll :: [String] -> [String]
checkAll = map (function . toInt)

toInt :: String -> [Integer]
toInt str = map read $ words str :: [Integer]

function :: [Integer] -> String
function a = show (fastPow (head a) (a!!1) (a!!2))

fastPow :: Integer -> Integer -> Integer -> Integer
fastPow base 1 m = mod base m
fastPow base pow m
  | even pow = mod (fastPow base (div pow 2) m ^ 2) m
  | odd  pow = mod (fastPow base (div (pow-1) 2) m ^ 2 * base) m

-- $ ./smendoz3
--   140955506 218970205 13774842 83214018 236056379 318518929 209684777
--   168426438 347237630 157905948 109157040 107686931 138454010 38615793
--   222338852 278401940 49937643 232853196 170378219 72979480 128246203
--   5565604 35600545 149268160 197309009
