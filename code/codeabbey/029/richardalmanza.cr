#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.56 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map_with_index {|x , y| {x.to_i, y + 1}}
args = args.sort_by {|x| x[0]}

args.each {|x| print "#{x[1]} "}

puts

# $ ./richardalmanza.cr
# 1 2 4 3 6 20 11 19 16 17 5 14 8 12 15 9 10 13 7 18
