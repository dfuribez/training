/*
$ gofmt -w lope391.go
$ go vet lope391.go
vet.exe: errors: 0; warnings: 0
$ go build -work lope391.go
WORK=/tmp/go-build233714584
*/
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func main() {

	absPath, _ := filepath.Abs("DATA.lst")
	arr, _ := readFileToStr(absPath)
	step, _ := strconv.Atoi(arr[0])
	strArr := strings.Split(arr[1], " ")
	var intArr []int
	var sortArr []int
	for i := 0; i <= step-1; i++ {
		v, _ := strconv.Atoi(strArr[i])
		intArr = append(intArr, v)
		sortArr = append(sortArr, v)
	}

	shellSort(sortArr)

	fmt.Printf("%v ", getIndex(sortArr[0], intArr))
	for i := 1; i <= step-2; i++ {
		fmt.Printf("%v ", getIndex(sortArr[i], intArr))
	}
	fmt.Printf("%v \n", getIndex(sortArr[step-1], intArr))
}

func readFileToStr(fn string) (arr []string, err error) {

	//Save file to buffer and close
	file, err := os.Open(fn)
	defer file.Close()

	if err != nil {
		return nil, err
	}

	//Cread reader buffer from file
	reader := bufio.NewReader(file)

	//Start reading buffer
	var line string
	for {
		line, err = reader.ReadString('\n')
		//Delete \n from last character of string
		if last := len(line) - 1; last >= 0 && line[last] == '\n' {
			line = line[:last]
		}
		if line != "" {
			arr = append(arr, line)
		}
		if err != nil {
			break
		}
	}

	if err != io.EOF {
		fmt.Printf("ERROR: %v\n", err)
	}

	return arr, nil
}

func shellSort(arr []int) int {

	n := len(arr)

	for gap := n / 2; gap > 0; gap = gap / 2 {
		for i := gap; i < n; i++ {
			tmp := arr[i]
			var j int
			for j = i; j >= gap && arr[j-gap] > tmp; j -= gap {
				arr[j] = arr[j-gap]
			}
			arr[j] = tmp
		}
	}

	return 0
}

func getIndex(n int, arr []int) int {
	for i, v := range arr {
		if v == n {
			return i + 1
		}
	}
	return -1
}

/*
./lope391
1 2 4 3 6 20 11 19 16 17 5 14 8 12 15 9 10 13 7 18
*/
