--[[
$ luacheck jarh1992.lua
Checking jarh1992.lua                             OK

Total: 0 warnings / 0 errors in 1 file
]]

local function reverse(s)
  local r = ""
  for _,c in utf8.codes(s) do
    r = utf8.char(c) .. r
  end
  return r
end

local data
data = io.lines("DATA.lst")
local opt = ""
for lin in data do
  opt = lin
end
local res = reverse(opt)
print(res)

--[[$ lua jarh1992.lua
sutcac tuoba ydrapoej flehs ffo reppus eraf no
]]
