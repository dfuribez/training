<?php
/*
PHP version 7.2.11.0

Linting with "SublimeLinter-PHP"

phpcs ciberstein.php

Compiling and linking using the "Command Windows prompt"

> C:\\..\php \\..\ciberstein.php
./output
*/
if(file_exists('DATA.lst')) {
  $file = fopen('DATA.lst', 'r');
  $S = ['Clubs','Spades','Diamonds','Hearts'];
  $R = ['2','3','4','5','6','7','8','9','10','Jack','Queen','King','Ace'];

  fscanf($file, "%d\n", $cards_count);

  $cards = explode(' ', fgets($file));
  $show = '';

  foreach ($cards as $card) {
    $show .= $R[$card % 13] . '-of-' . $S[(int)($card / 13)] . ' ';
  }
  echo trim($show);
}
else
  echo 'Error DATA.lst not found';
/*
./ciberstein.php
8-of-Clubs Queen-of-Spades 9-of-Diamonds 3-of-Diamonds Jack-of-Spades
2-of-Diamonds 8-of-Hearts 8-of-Diamonds 7-of-Diamonds Jack-of-Diamonds
Jack-of-Hearts 4-of-Spades 4-of-Diamonds 5-of-Diamonds Queen-of-Hearts
King-of-Hearts 6-of-Diamonds 4-of-Hearts Jack-of-Clubs 10-of-Diamonds
3-of-Spades Ace-of-Clubs Queen-of-Diamonds King-of-Clubs 10-of-Spades
Ace-of-Hearts 2-of-Spades 6-of-Clubs
*/
?>
