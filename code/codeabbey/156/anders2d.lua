--[[
$ ./luacheck.exe anders2d.lua
Checking anders2d.lua                             OK
Total: 0 warnings / 0 errors in 1 file
]]

require 'io'

local data
local dataArray = {}
local nCases = 0

data = io.lines("DATA.lst")


local function printArray(array)
    for i = 1, #array do
        io.write(array[i])
    end
    io.write(" ")
end

local function tableClone(org)
    local clone = {}
    for i = 1, #org do
        table.insert(clone,org[i])
    end
    return clone

end

local function swapPosition(array,pos1, pos2)
    local tmpValue
    tmpValue = array[pos1]
    array[pos1]=array[pos2]
    array[pos2]=tmpValue

    return array
end

local function checkSum(array)
    local sum = 0 ;
    for iterator = 16 , 1 , -4 do

        local first = array[iterator]
        local second = array[iterator-1]
        local third = array[iterator-2]
        local fourth = array[iterator-3]

        local block

        second = second * 2
        fourth = fourth * 2
        if second >= 10 then
            second = second - 9
        end
        if fourth >= 10 then
            fourth = fourth - 9
        end
        block =  first + second + third + fourth
        sum = sum +block

    end

    if (sum%10) == 0 then
        return true
    end
    return false
end

local function fixCardNumber(cardNumber)
    local eachDataArray = {}
    local positionQuestionMark = 0

    local subStr
    for i = 1, #cardNumber do
        subStr = cardNumber:sub(i, i)
        table.insert(eachDataArray,i,subStr)
        if subStr == "?" then
            positionQuestionMark = i
        end
    end

    if positionQuestionMark > 0 then
        for i= 0, 9 do
            eachDataArray[positionQuestionMark] = i

            if checkSum(eachDataArray) then
                break
            end
        end

    else
        local tmpEachDataArray = tableClone(eachDataArray)
        for i=1, 15 do
            tmpEachDataArray = swapPosition(tmpEachDataArray,i,i+1)
--~             printArray(tmpEachDataArray)
            if checkSum(tmpEachDataArray) then
                eachDataArray=tmpEachDataArray
                break
            end
            tmpEachDataArray = tableClone(eachDataArray)
        end

    end
    printArray(eachDataArray)
end

local function main()
    for line in data do
        nCases=nCases+1
        table.insert(dataArray, line)
    end

    for i = 1, nCases do
        fixCardNumber(dataArray[i])
    end
end

main()

--[[
$ lua -e "io.stdout:setvbuf 'no'" "anders2d.lua"
2425357742588228 8571624807864011 3768496009757275 8887733133631116
4502078662367573 2249599736593359 4225530727413525 3113867071995682
9650801794359617 4189098394652720 9375681389450722 1276249332501053
6147257700761064 5870826087400697 6523534219623454 2409828521618014
3727742668334488 9929165033733817 1916229440140578 4727013844493335
9071640813349378 2502614756478134 7412201775550909 7659168956315950
7947249853600686 9826488803298041 6702849288793704 2148364442484761
2681322017276510 2419083146638397 5922579787706974 3659879047560417
4163920138833250 9513047553632375 3762231955202059 3700913888005676
3192323568511016 3156937570491331 4128195282360988 2540528476656569
8411429882283671 2730542624665539 9278115798505519 6003879395168082
5725979790776858 9034247817203035 3084328876480565 5916968511797706
8341723131591560 7042490578887869
]]
