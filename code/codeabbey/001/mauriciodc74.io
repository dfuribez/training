#!/usr/local/bin/io
/*
 Scripts are executed without compilation.
 No linter available for IO
*/

data := File with("DATA.lst")
data openForReading
values := data readLine
list := values split
a := list at(0) asNumber
b := list at(1) asNumber
sum := a + b
sum print
data close

/*
 $ io mauriciodc74.io
 18772
*/
