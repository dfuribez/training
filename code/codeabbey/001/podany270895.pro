:- initialization(main).

main:-
  read(A),
  read(B),
  sum(A,B),
  halt.

sum(A,B):-
  R is A+B,
  write(R).
