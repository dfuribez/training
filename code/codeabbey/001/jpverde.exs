# $ mix credo
# Checking 3 source files ...
# Analysis took 0.1 seconds (0.03s to load, 0.1s running checks)
# 3 mods/funs, found no issues.
# $ iex -S mix

defmodule Sum do
  def sum do
    {:ok, contents} = File.read("DATA.lst")
    nums = contents |> String.split([" ", "\r\n"], trim: true)
    dat = nums |> Enum.map(&String.to_integer/1)
    IO.puts("Result of sum = #{Enum.sum(dat)}")
  end
end

# $ iex(1)> Sum.sum
# Result of sum = 18772
# :ok
