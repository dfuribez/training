#! /usr/bin/crystal
# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.57 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

fluid_code = true

oput = ""

if fluid_code
  args = File.read("DATA.lst").split
else
  args = ARGV.map { |x| x.split }.flatten
end

args = args[1..].map { |x| x.to_i }

(0...args.size).each do |x|
  celsius = (args[x] - 32) / 1.8
  oput = "#{oput} #{celsius.round.to_i}"
end

puts oput

# $ ./richardalmanza.cr --
# 0 247 116 26 42 38 146 57 139 77 94 117 309 232 264 11 138 169 278 132 126
# 263 293 138 262 123 170 298 154 11 121 154 257 237
