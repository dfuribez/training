let printArray arr = Array.iter (Printf.printf "%i ") arr;;
let inp=[[86744;84408];[92822;37443];[68797;98036];[46834;75988];[32020;15103];[18509;85734];[97025;77201];[99506;87364];[32107;69739];[61298;50465];[48086;46699];[99225;19669];[12235;35120];[84029;35571];[98162;94951];[93957;84907];[79360;86780];[22351;48157];[84816;59186];[24145;16837];[64290;32655];[92572;61315];[99857;92078];[48680;31964];[61817;99979];[72430;99904];[46678;71656];[19574;48914]] in
let res=[| 0 ; 0; 0|] in
for i=0 to (List.length inp) -1     do
        let r= ref 1 in
        let sprev= ref 1 in
      let scur= ref 0 in
      let tprev = ref 0 in
      let tcur= ref 1 in     
        let h= ref 1 in
        let n= ref 1 in
        let l= ref 1 in
        let snext=ref 1 in
        let tnext=ref 2 in
        let x=ref (List.nth (List.nth inp (i)) 0) in
        let y=ref (List.nth (List.nth inp (i)) 1) in
        while !r <> 0 do
            h := !r;
            n:= !snext;
            l:= !tnext;
            r := (!x mod !y) ;
            let q=(!x- !r)/ !y in
          snext:= (!sprev-(q* !scur));
            tnext:=(!tprev-(q* !tcur));
            if !r=0 then 
                begin
                res.(0) <- !h;
                res.(1) <- !n;
                res.(2) <- !l;
                printArray res
                end
            else 
                begin
                x := !y;
                y := !r;
                sprev:=!scur ;
                scur:=!snext ;
                tprev:=!tcur ;
                tcur:=!tnext ;    
                end    
        done;
    done;
