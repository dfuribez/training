/*
$ cargo clippy
Checking rust v0.1.0 (/home/phantom/Desktop/fluid/finished/24/rust)
Finished dev [unoptimized + debuginfo] target(s) in 0.19s
*/

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() -> std::io::Result<()> {
  let file = File::open("src/DATA.lst")?;
  let mut buf_reader = BufReader::new(file);
  let mut contents = String::new();
  buf_reader.read_to_string(&mut contents)?;
  let lines = contents.lines();
  let mut inp = Vec::new();
  for l in lines {
    inp.push(l);
  }
  let _inp1: u32 = inp[0].parse().unwrap();
  let mut inp2 = Vec::new();
  for j in inp[1].split_whitespace() {
    inp2.push(j.parse().unwrap());
  }
  let mut counter: u8 = 0;
  let mut rsp = Vec::new();
  let mut vals = Vec::new();
  let mut aux: u32;
  let mut aux2: u32;
  for i in inp2.iter() {
    aux = *i;
    loop {
      aux2 = (aux * aux / 100) % 10000;
      if vals.iter().find(|&&x| x == aux) != None {
        rsp.push(counter.to_string());
        break;
      }
      vals.push(aux);
      counter += 1;
      aux = aux2;
    }
    counter = 0;
    vals.clear();
  }
  println!("{:?}", rsp.join(" "));
  Ok(())
}

/*
$ cargo run
Compiling rust v0.1.0 (/home/phantom/Desktop/fluid/finished/24/rust)
Finished dev [unoptimized + debuginfo] target(s) in 0.29s
Running `target/debug/rust`
"107 105 104 100 101 110 97 103 108 103 101 106"
*/
