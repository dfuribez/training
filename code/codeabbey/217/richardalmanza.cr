#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 21.13 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

BODIES = ("A".."Y").to_a

def join_cycles(cycles, current_state, swaps)
  if cycles.size > 1
    counter = 0
    max = cycles[0].size * cycles[1].size
    counter_a = 0
    counter_b = 0

    to_swap = {cycles[0][counter_a][0], cycles[1][counter_b][0]}
    validswap = !(to_swap.in?(swaps) || to_swap.reverse.in?(swaps))
    until validswap
      counter += 1
      counter_a = counter % cycles[0].size
      counter_b = (counter // cycles[0].size) % cycles[1].size

      to_swap = {cycles[0][counter_a][0], cycles[1][counter_b][0]}
      validswap = !(to_swap.in?(swaps) || to_swap.reverse.in?(swaps))
      break if counter > max
    end

    return nil if counter > max
    swaps << to_swap

    cycles[1].concat cycles[0]
    swap_soul(to_swap[0], to_swap[1], cycles[1], show: true)

    cycles[1..].each do |cycle|
      cycle.each { |bm| current_state << bm }
    end

    return true
  end
end

def solve_cycle(cycle, current_state, unsolved)
  helper_a = current_state.pop
  helper_b = current_state.pop

  cycle << helper_a

  (0...cycle.size - 2).each do |i|
    body_a = cycle[i][0]
    body_b = helper_a[0]

    swap_soul(body_a, body_b, cycle, show: true)
  end

  clean_cycle(cycle, current_state)
  cycle << helper_b

  swap_soul(cycle[1][0], helper_b[0], cycle, show: true)
  swap_soul(cycle[1][0], helper_a[0], cycle, show: true)
  swap_soul(cycle[0][0], helper_b[0], cycle, show: true)

  if unsolved == 1
    swap_soul(helper_b[0], helper_a[0], cycle, show: true)
  else
    ha = (get_bm(helper_b[0], cycle) || {"", ""})
    hb = (get_bm(helper_a[0], cycle) || {"", ""})
    current_state << hb
    current_state << ha
    cycle.delete ha
    cycle.delete hb
  end

  clean_cycle(cycle, current_state)
end

def get_bm(body, list)
  list.find { |bm| bm[0] == body }
end

def clean_cycle(cycle, current_state)
  to_delete = [] of Tuple(String, String)
  cycle.each do |b_m|
    if b_m[0].downcase == b_m[1]
      current_state << b_m
      to_delete << b_m
    end
  end
  to_delete.each do |pair|
    cycle.delete pair
  end
end

def arrange_cycles(current_state)
  cycles = [] of Array(Tuple(String, String))
  finishied = false
  i_cycle = 0

  until finishied
    cycles << [current_state[0]]
    current_state.delete_at 0

    i_body = current_state.index { |x| x[0] == cycles[i_cycle][-1][1].upcase }
    while i_body
      cycles[i_cycle] << current_state[i_body]
      current_state.delete_at i_body

      i_body = current_state.index { |x| x[0] == cycles[i_cycle][-1][1].upcase }
    end

    i_cycle += 1
    finishied = true if current_state.empty?
  end

  cycles.each do |cycle|
    clean_cycle(cycle, current_state)
  end

  cycles.sort_by { |x| x.size }.uniq.[1..]
end

def swap_soul(body_a, body_b, list, show = false)
  i_a = list.index { |x| body_a == x[0] } || 999
  i_b = list.index { |x| body_b == x[0] } || 999

  temp = list[i_a][1]
  list[i_a] = {body_a, list[i_b][1]}
  list[i_b] = {body_b, temp}

  print "#{body_a}-#{body_b} " if show
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split(/[- \n]/, remove_empty: true) if fluid
args = args[0].split(/[- \n]/, remove_empty: true) if !fluid

args = args[2..]

current_state = BODIES.map { |x| {x, x.downcase} }
swaps = [] of Tuple(String, String)

(0...args.size).step(2).each do |x|
  swap_soul(args[x], args[x + 1], current_state)
  swaps << {args[x], args[x + 1]}
end

cycles = arrange_cycles(current_state)
one_cycle = join_cycles(cycles, current_state, swaps)
while one_cycle
  cycles = arrange_cycles(current_state)
  one_cycle = join_cycles(cycles, current_state, swaps)
end

solve_cycle(cycles[0], current_state, cycles.size)
current_state.sort_by! { |x| x[0] }

puts

# $ ./richardalmanza.cr
# G-B B-A A-Y O-Y G-Y C-Y D-Y H-Y Q-Y B-Y I-Y
# J-Y F-Y E-Y N-Y M-Y P-Y L-Y K-X K-Y A-X X-Y
